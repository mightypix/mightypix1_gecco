onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib untrigRO_selectIO_opt

do {wave.do}

view wave
view structure
view signals

do {untrigRO_selectIO.udo}

run -all

quit -force
