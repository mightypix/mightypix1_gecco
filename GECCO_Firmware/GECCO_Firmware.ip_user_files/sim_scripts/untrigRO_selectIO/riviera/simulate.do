onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+untrigRO_selectIO -L xil_defaultlib -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.untrigRO_selectIO xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {untrigRO_selectIO.udo}

run -all

endsim

quit -force
