onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib async_fifo_ftdi_opt

do {wave.do}

view wave
view structure
view signals

do {async_fifo_ftdi.udo}

run -all

quit -force
