// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.2 (lin64) Build 1909853 Thu Jun 15 18:39:10 MDT 2017
// Date        : Sat Aug 27 20:44:50 2022
// Host        : ipe-iperic02 running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/hzhang/MPROC/MPROC/GECCO_Firmware/GECCO_Firmware.src/ip/clk_wiz_1/clk_wiz_1_stub.v
// Design      : clk_wiz_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a200tsbg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module clk_wiz_1(clk_out640, clk_out128, clk_out400, clk_out4, 
  clk_out5, clk_out6, reset, locked, clk_in1)
/* synthesis syn_black_box black_box_pad_pin="clk_out640,clk_out128,clk_out400,clk_out4,clk_out5,clk_out6,reset,locked,clk_in1" */;
  output clk_out640;
  output clk_out128;
  output clk_out400;
  output clk_out4;
  output clk_out5;
  output clk_out6;
  input reset;
  output locked;
  input clk_in1;
endmodule
