/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`timescale 1ns / 1ps
`define MASTER
//////////////////////////////////////////////////////////////////////////////////
// Company:     KIT-ADL
// Engineer:    Rudolf Schimassek
// 
// Create Date: 05.09.2019
// Design Name: 
// Module Name: Time Stamp Generator for untriggered Readout on ATLASPix3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
//
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module untriggered_TSGenerator(

     `ifndef MASTER
    

        input sync_rst_input,  
    
    
    `endif

	input  wire 		clock,
	input  wire 		reset,
	input  wire 		enable,
	
	input  wire [7:0] 	tsdiv,
	input  wire [7:0] 	ts2div,
	input  wire [7:0] 	tsphase,
	
	output wire [39:0] 	tsout,
	output wire [31:0]	ts2out,
	output wire 		syncReset,
	output wire         overflowsync,
	output reg injtrigger
);


reg fast_syncReset_neg;

reg  [7:0]  bincounter;
reg  [31:0] overflowcounter;
reg  [7:0]  bincounter2;
reg  [23:0] overflowcounter2;


//split counters to enable faster clocking:
reg  [3:0] tsdiv_cnt1;
reg  [3:0] tsdiv_cnt2;
reg  [3:0] ts2div_cnt1;
reg  [3:0] ts2div_cnt2;
//name for the concatenation of the split counters:
wire [7:0] tsdiv_cnt;
wire [7:0] ts2div_cnt;
assign tsdiv_cnt  = {tsdiv_cnt2, tsdiv_cnt1};
assign ts2div_cnt = {ts2div_cnt2, ts2div_cnt1};

reg pre_fast_syncReset;
reg fast_syncReset;
//assign syncReset = fast_syncReset & enable;

//assign syncReset = pre_fast_syncReset;

assign syncReset = fast_syncReset_neg;//negedge

reg pre_overflowsync_reg;
reg overflowsync_reg;
assign overflowsync = overflowsync_reg & enable;
assign tsout  = (enable)? {overflowcounter,  bincounter } : 40'b0;
assign ts2out = (enable)? {overflowcounter2, bincounter2} : 32'b0;

always @(posedge clock) begin
	if(reset) begin
		tsdiv_cnt1   	   <=  4'b0;
		tsdiv_cnt2   	   <=  4'b0;
		ts2div_cnt1  	   <=  4'b0;
		ts2div_cnt2  	   <=  4'b0;
		bincounter  	   <=  8'b0;
		bincounter2 	   <=  8'b0;
		overflowcounter    <= 32'b0;
		overflowcounter2   <= 24'b0;
		pre_fast_syncReset <=  1'b0;
	end
	else if(enable) begin
		//TS clock division:
		
		//if(overflows1_1[1:0] == 2'b00) if(ts1_cnt == 10'd0) injtrigger <= 1;
        //if(overflows1_1[1:0] == 2'b00) if(ts1_cnt == 10'd16) injtrigger <= 0;
                    
		
		if(tsdiv_cnt < tsdiv)
			if(tsdiv_cnt1[3:0] == 4'b1111) begin
				tsdiv_cnt1 <= 4'b0;
				tsdiv_cnt2 <= tsdiv_cnt2 + 4'b1;
			end
			else
				tsdiv_cnt1 <= tsdiv_cnt1 + 4'b1;
		else
			{tsdiv_cnt2,tsdiv_cnt1} <= 8'b0;
		
		//TS2 clock division:
		if(ts2div_cnt < ts2div) begin
			if(ts2div_cnt1[3:0] == 4'b1111) begin
				ts2div_cnt1 <= 0;
				ts2div_cnt2 <= ts2div_cnt2 + 4'b1;
			end
			else
				ts2div_cnt1 <= ts2div_cnt1 + 4'b1;
		end
		else
			{ts2div_cnt2,ts2div_cnt1} <= 8'b0;
		
		//TS update:
		
		
		`ifdef MASTER
       
   
       if(tsdiv_cnt == tsphase) begin
         
             if(pre_fast_syncReset == 1) begin 
             
             bincounter         <=  8'b0;
             //bincounter2        <=  8'b0;
             overflowcounter    <= 32'b0;
             //overflowcounter2   <= 24'b0;
             
             end
             
             else begin
             
                 
                     bincounter <= bincounter + 8'b1;
                     //timestamp extension
                     if(bincounter[7:0] == 8'hFF)
                                 overflowcounter  <= overflowcounter + 32'b1;
                                 
                         end//not reset        
                             
                     end//phase
        
        `else
        
        if(tsdiv_cnt == tsphase) begin
        
            if(sync_rst_input == 1) begin 
            
            bincounter  	   <=  8'b0;
            //bincounter2        <=  8'b0;
            overflowcounter    <= 32'b0;
            //overflowcounter2   <= 24'b0;
            
            end
            
            else begin
            
                
                    bincounter <= bincounter + 8'b1;
                    //timestamp extension
                    if(bincounter[7:0] == 8'hFF)
                                overflowcounter  <= overflowcounter + 32'b1;
                                
                        end//not reset        
                            
                    end//phase
       
       
       `endif
		
		

		
		
		
		
		
		//TS2 update:
		//		syncReset:
		if(tsdiv_cnt == tsphase && {overflowcounter[15:0],bincounter[7:0]} == 24'hFFFFFF) begin
			bincounter2      <=  8'b0; //reset TS2 as sync reset is sent
			overflowcounter2 <= 24'b0; //     - '' -
		end
		//		increment:
		else if(ts2div_cnt == tsphase) begin
			bincounter2 <= bincounter2 + 8'b1;
			if(bincounter2 == 8'hFF)
				overflowcounter2 <= overflowcounter2 + 24'b1;
		end
		
		
		if(tsdiv_cnt == tsphase && {overflowcounter[5:0],bincounter[7:0]} == 14'h3FFE)
            injtrigger <= 1;
        else if(tsdiv_cnt == tsphase)
            injtrigger <= 0;
		/*
		if(tsdiv_cnt == tsphase && {overflowcounter[3:0],bincounter[7:0]} == 12'hFFE)
            injtrigger <= 1;
        else if(tsdiv_cnt == tsphase)
            injtrigger <= 0;
          */
          
            
        //if(tsdiv_cnt == tsphase && {overflowcounter[3:0],bincounter[7:0]} == 12'h010)
            //injtrigger <= 0;
		
		/*
		if(tsdiv_cnt == tsphase && {overflowcounter[1:0],bincounter[7:0]} == 10'h3FE)
            injtrigger <= 1;
            
        if(tsdiv_cnt == tsphase && {overflowcounter[1:0],bincounter[7:0]} == 10'h010)
            injtrigger <= 0;

		*/
		
		
		if(tsdiv_cnt == tsphase && {overflowcounter[15:0],bincounter[7:0]} == 24'hFFFFFE)
			pre_fast_syncReset <= 1;
		else if(tsdiv_cnt == tsphase)
			pre_fast_syncReset <= 0;
		
		
		/*	
		if(tsdiv_cnt == tsphase && {overflowcounter[5:0],bincounter[7:0]} == 14'h3FFE)//ivan 21 original rudolf
                pre_fast_syncReset <= 1;
            else if(tsdiv_cnt == tsphase)
                pre_fast_syncReset <= 0;
                
                */

			
			
			
        if(tsdiv_cnt == tsphase && {overflowcounter[1:0],bincounter[7:0]} == 10'h3FE)
            pre_overflowsync_reg <= 1;
        else if(tsdiv_cnt == tsphase)
            pre_overflowsync_reg <= 0;
		
	end
end

reg  [2:0] ntsdiv_cnt1;
reg  [3:0] ntsdiv_cnt2;
wire [6:0] ntsdiv_cnt;
assign ntsdiv_cnt  = {ntsdiv_cnt2, ntsdiv_cnt1};
always @(negedge clock) begin
    if(reset) begin
        ntsdiv_cnt1 <= 0;
        ntsdiv_cnt2 <= 0;
    end
    else begin
    
    fast_syncReset_neg <= pre_fast_syncReset;
    
        if(tsdiv_cnt == tsphase) begin
            {ntsdiv_cnt2,ntsdiv_cnt1} <= 7'b0;
        end
        else begin
        //TS clock division:
            if(tsdiv_cnt < tsdiv) begin
                if(ntsdiv_cnt1[2:0] == 3'b111) begin
                    ntsdiv_cnt1 <= 3'b0;
                    ntsdiv_cnt2 <= ntsdiv_cnt2 + 4'b1;
                end
                else
                    ntsdiv_cnt1 <= ntsdiv_cnt1 + 3'b1;
            end
        end
            

        if(ntsdiv_cnt[6:0] == tsphase[7:1]) begin
	       fast_syncReset <= pre_fast_syncReset;
	       overflowsync_reg <= pre_overflowsync_reg;
	    end
	end
end

endmodule