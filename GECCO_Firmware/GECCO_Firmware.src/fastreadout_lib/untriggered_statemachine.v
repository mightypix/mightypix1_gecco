/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//////////////////////////////////////////////////////////////////////////////////
// Company:     KIT-ADL
// Engineer:    Rudolf Schimassek
// 
// Create Date: 05.09.2019
// Design Name: 
// Module Name: State Machine for untriggered Readout on ATLASPix3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
//
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module untriggered_statemachine(
	input  wire 		clock,
	input  wire 		reset,
	input  wire 		enable,
	input  wire         debug_output,
	input  wire         data_mux,
	
	input  wire [18:0]  debug_data,
	input  wire 		commaword,
	input  wire [7:0] 	datain,
	input  wire [39:0]	ts_from_SM,
	input  wire [39:0]  ts2_from_SM,
	input  wire [39:0]  trigger_ts,
	input  wire [23:0]  trigger_id,
	
	//FIFO output:
	output reg  [63:0] 	dataset,
	output wire 		FIFO_wr_clk,
	output reg			FIFO_wr_enable,
	//state machine debug:
	output wire [3:0] 	debug_state,
	output wire			debug_found_empty_output,
	output wire			debug_found_data_start
);


reg  [3:0] 	state;
reg			empty_output;
reg			data_start;

assign debug_state[3:0]         = state[3:0];
assign debug_found_empty_output = empty_output;
assign debug_found_data_start   = data_start;

assign FIFO_wr_clk = enable & clock;

reg ignore = 1;
reg [1:0] bincnt = 0;

//data from the last data set:
reg [7:0]  data_column;
reg [7:0]  data_col2;
reg [8:0]  data_row;
reg [39:0] data_ts;
reg [39:0] data_ts2;
reg [15:0] data_bincounter;
reg [7:0]  data_tstodet;
//registers for extending the time stamp:
reg [29:0] extended_ts;
reg [29:0] extended_ts_minusone;
reg [32:0] extended_ts2;
reg [32:0] extended_ts2_minusone;

//Gray Decoding:
wire [9:0] ts_decoded;
assign ts_decoded = {data_ts[9],
					 data_ts[9]^data_ts[8],
					 data_ts[9]^data_ts[8]^data_ts[7],
					 data_ts[9]^data_ts[8]^data_ts[7]^data_ts[6],
					 data_ts[9]^data_ts[8]^data_ts[7]^data_ts[6]^data_ts[5],
					 data_ts[9]^data_ts[8]^data_ts[7]^data_ts[6]^data_ts[5]^data_ts[4],
					 data_ts[9]^data_ts[8]^data_ts[7]^data_ts[6]^data_ts[5]^data_ts[4]^data_ts[3],
					 data_ts[9]^data_ts[8]^data_ts[7]^data_ts[6]^data_ts[5]^data_ts[4]^data_ts[3]^data_ts[2],
					 data_ts[9]^data_ts[8]^data_ts[7]^data_ts[6]^data_ts[5]^data_ts[4]^data_ts[3]^data_ts[2]^data_ts[1],
					 data_ts[9]^data_ts[8]^data_ts[7]^data_ts[6]^data_ts[5]^data_ts[4]^data_ts[3]^data_ts[2]^data_ts[1]^data_ts[0]};
wire [6:0] ts2_decoded;
assign ts2_decoded = {data_ts2[6],
					  data_ts2[6]^data_ts2[5],
					  data_ts2[6]^data_ts2[5]^data_ts2[4],
					  data_ts2[6]^data_ts2[5]^data_ts2[4]^data_ts2[3],
					  data_ts2[6]^data_ts2[5]^data_ts2[4]^data_ts2[3]^data_ts2[2],
					  data_ts2[6]^data_ts2[5]^data_ts2[4]^data_ts2[3]^data_ts2[2]^data_ts2[1],
					  data_ts2[6]^data_ts2[5]^data_ts2[4]^data_ts2[3]^data_ts2[2]^data_ts2[1]^data_ts2[0]};

//Debug output delay registers:
reg[18:0] debug_store1;
reg[18:0] debug_store2;
reg[18:0] debug_store3;
reg[18:0] debug_store4;
reg[18:0] debug_store5;

//actual data receiving:
always @(posedge clock) begin //(posedge clock) begin
	if(reset) begin
		state  				  <= 0;
		empty_output		  <= 0;
		data_start			  <= 0;
		ignore 				  <= 1;
		bincnt	 			  <= 0;
		FIFO_wr_enable 		  <= 0;
		
		data_column		 	  <= 0;
		data_col2             <= 0;
		data_row 			  <= 0;
		data_ts				  <= 0;
		data_ts2			  <= 0;
		data_bincounter 	  <= 0;
		data_tstodet		  <= 0;
		extended_ts			  <= 0;
		extended_ts2 		  <= 0;
		extended_ts_minusone  <= 0;
		extended_ts2_minusone <= 0;
	end
	else if(enable) begin
		//update time stamp extension:
		if(ts_from_SM[39:10] != extended_ts) begin
			extended_ts[29:0] 			<= ts_from_SM[39:10];
			extended_ts_minusone[29:0] 	<= extended_ts[29:0];
		end
		//update time stamp 2 extension:
		if(ts2_from_SM[39:7] != extended_ts2) begin
			extended_ts2[32:0] 			<= ts2_from_SM[39:7];
			extended_ts2_minusone[32:0] <= extended_ts2[32:0];
		end
	
	   if(debug_output) begin
	       debug_store1 <= debug_data;
	       debug_store2 <= debug_store1;
	       debug_store3 <= debug_store2;
	       debug_store4 <= debug_store3;
	       debug_store5 <= debug_store4;
	   end
	   
		//SendCounter1 State (start):
		if(commaword && datain == 8'h3C) begin
			bincnt <= 3;
			state  <= 0;
		end
		//SendCounter Payload:
		else if(bincnt > 0) begin
			FIFO_wr_enable <= bincnt > 1;	//last byte is empty
			dataset <= {2'b0,bincnt,state,32'b0,datain[7:0]};
			bincnt <= bincnt - 2'b1;
		end
		//LoadColumn2 State
		if(commaword && datain == 8'h1C) begin
			ignore 			 <= 1;
			FIFO_wr_enable 	 <= 0;
			data_start <= 1;
		end
		//LoadColumn2 PayLoad (8'hAA; don't care):
		else if(ignore) begin
			ignore         <= 0;
			state          <= 1;
			FIFO_wr_enable <= 0;
		end
		//SyncState content (don't care):
		else if(commaword && datain == 8'hBC) begin
			FIFO_wr_enable     <= 0;
			empty_output <= 1;
		end
		//actual data of interest:
		else begin
			if(state == 12)
				state <= 5;
			else
				state <= state + 4'b1;
				
			data_start   <= 0;
			empty_output <= 0;
			
			if(~data_mux) begin
			    dataset[63:56] <= {4'b0, state[3:0]};
			    dataset[55:48] <= datain[7:0];
			end
			
			if(debug_output && ~data_mux)
			     FIFO_wr_enable <= 1;
			
			case(state)
			    1: begin
					//empty data
					dataset[47:0] <= {5'b0, debug_store2[18:0], 5'b0, debug_store1[18:0]};
			        if(~debug_output)
			            FIFO_wr_enable 		<= 0; 
				end
				2: begin
					dataset[47:0] <= {5'b0, debug_store5[18:0], 5'b0, debug_store4[18:0]};
					data_bincounter[15:8]	<= datain[7:0];
				end
				3: begin
					dataset[47:0] <= {5'b0, debug_store5[18:0], 5'b0, debug_store4[18:0]};
					data_bincounter[7:0]  	<= datain[7:0];
				end
				4: begin
					dataset[47:0] <= {5'b0, debug_store5[18:0], 5'b0, debug_store4[18:0]};
					data_tstodet[7:0] 		<= datain[7:0];
				end
				5: begin
					dataset[47:0] <= {5'b0, debug_store5[18:0], 5'b0, debug_store4[18:0]};
					if(data_mux)
					   FIFO_wr_enable       <= 0;
				end
				6: begin
					dataset[47:0]    		<= {8'h11, 8'h22, 8'h33, 8'h44, 8'h55, 8'h66}; //useful for data alignment
					data_column[7:0] 		<= datain[7:0];
					if(~debug_output && ~data_mux)
					    FIFO_wr_enable     	<= 1;
				end
				7: begin
					dataset[47:0] 			<= {8'h77, 8'h88, 8'd99, 8'haa, 8'hbb, 8'hcc}; //useful for data alignment
					data_ts2[0]   			<= datain[0];
				end
				8: begin
					dataset[47:0] 			<= {8'b0, data_tstodet[7:0], 8'b0, trigger_id[23:0]};
					data_ts2[6:1] 			<= {datain[2],datain[3],datain[4],datain[5],datain[6],datain[7]};
					data_row[1:0]           <= {datain[0],datain[1]};
				end
				9: begin
					dataset[47:0] 			<= {4'b0, trigger_id[1:0], 2'b0, trigger_ts[39:0]};
					data_ts[9:8]  			<= datain[1:0];
					data_col2[7:0]          <= data_column[7:0];  //test to try to fix wrong address using datamux
				end
				10: begin
                    if(ts2_from_SM[6:0] > ts2_decoded[6:0]) begin
                        data_ts2[39:0]      <= {extended_ts2[32:0], ts2_decoded[6:0]};
                        if(~data_mux)
                            dataset[47:0]   <= {8'b0, extended_ts2[32:0], ts2_decoded[6:0]};
                    end
                    else begin
                        data_ts2[39:0]      <= {extended_ts2_minusone[32:0], ts2_decoded[6:0]};
                        if(~data_mux)
                            dataset[47:0]   <= {8'b0, extended_ts2_minusone[32:0], ts2_decoded[6:0]};
                    end
					if(data_mux) begin
					    dataset[63:0]       <= {8'd1,trigger_ts[39:0], trigger_id[15:0]};
					    FIFO_wr_enable      <= 1;
					end
					data_ts[7:0] 			<= datain[7:0];
				end
				11: begin
					//empty data
					if(ts_from_SM[9:0] > ts_decoded[9:0]) begin
						data_ts[39:0] 		<= {extended_ts[29:0], ts_decoded[9:0]};
						if(~data_mux)
						    dataset[47:0] 	<= {8'b0, ts_from_SM[39:0]};
						    //dataset[47:0] 	<= {8'b0, extended_ts[29:0], ts_decoded[9:0]};
						    
						    
					end
					else begin
						data_ts[39:0] 		<= {extended_ts_minusone[29:0], ts_decoded[9:0]};
						if(~data_mux)
						    //dataset[47:0] 	<= {8'b0, extended_ts_minusone[29:0], ts_decoded[9:0]};
						    dataset[47:0] 	<= {8'b0, ts_from_SM[39:0]};
					end
					if(data_mux)
					    dataset[63:0]       <= {8'd2, data_ts2[39:0], data_col2[7:0], trigger_id[23:16]};
					    //dataset[63:0]       <= {8'd2, data_ts2[39:0], data_column[7:0], trigger_id[23:16]};
				end
				12: begin
					if(~data_mux)
					    dataset[47:0] 		<= {8'b0, data_bincounter[15:0], 7'b0, 
					                            datain[0],datain[1],datain[2],datain[3],datain[4],datain[5],datain[6], 
					                            data_row[1:0], data_column[7:0]};
					else
					    dataset[63:0]       <= {8'd3, 7'd0, 
					                            datain[0],datain[1],datain[2],datain[3],datain[4],datain[5],datain[6],
					                            data_row[1:0], data_ts[39:0]};
					data_row[8:2] 			<= {datain[0],datain[1],datain[2],datain[3],datain[4],datain[5],datain[6]};
				end
				default:
					//this should not happen!
					dataset[47:0] 			<= 48'h123456789abc;
			endcase
		end
	end
end

endmodule