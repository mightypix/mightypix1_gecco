/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:     KIT-ADL
// Engineer:    Rudolf Schimassek
// 
// Create Date: 05.09.2019
// Design Name: 
// Module Name: Byte Clock Generation for untriggered Readout on ATLASPix3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
//
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ByteClockGenerator(
	input  wire 	  clock,
	input  wire 	  reset,
	
	input  wire [2:0] phaseshift_input_clock,
	input  wire [2:0] phaseshift_output_clock,
	
	output wire		  bit_clock,
	output wire		  block_clock,
	output reg 		  block_clock_posedge,
	output wire [8:0] delayed_clock
);

//clock delay without a faster clock:
//wire [8:0] delayed_clock;
/*
BUFIO bd1(
    .I(clock),
    .O(delayed_clock[0])
);

BUFIO bd2(
    .I(delayed_clock[1]),
    .O(delayed_clock[2])
);
BUFIO bd3(
    .I(delayed_clock[2]),
    .O(delayed_clock[3])
);
BUFIO bd4(
    .I(delayed_clock[3]),
    .O(delayed_clock[4])
);
*/

assign delayed_clock[0] = ~clock;
assign delayed_clock[1] = ~delayed_clock[0];
assign delayed_clock[2] = ~delayed_clock[1];
assign delayed_clock[3] = ~delayed_clock[2];
assign delayed_clock[4] = ~delayed_clock[3];
assign delayed_clock[5] = ~delayed_clock[4];
assign delayed_clock[6] = ~delayed_clock[5];
assign delayed_clock[7] = ~delayed_clock[6];
assign delayed_clock[8] = ~delayed_clock[7];

//clock phase shifting for data alignment:
wire localclock;
assign localclock = (phaseshift_input_clock[2])?
                     ((phaseshift_input_clock[1])? 
                        ((phaseshift_input_clock[0])? delayed_clock[7] : delayed_clock[6])
                      : ((phaseshift_input_clock[0])? delayed_clock[5] : delayed_clock[4]))
                   : ((phaseshift_input_clock[1])? 
                                           ((phaseshift_input_clock[0])? delayed_clock[3] : delayed_clock[2])
                                         : ((phaseshift_input_clock[0])? delayed_clock[1] : delayed_clock[0])); //clock));
//clock divider for 10bit output
reg deser_block_clock = 0;
reg deser_block_clock_edge = 0;  //edge marker for clk40
reg [2:0] clock_div = 3'b0;
reg posbitclk;
reg negbitclk;
reg blockclk;

//clock division:
always @(posedge localclock) begin
    if(reset) begin
        blockclk <= (phaseshift_output_clock[2:0] > 4 || phaseshift_output_clock[2:0] < 2);
        if(phaseshift_output_clock[2:0] > 4)
            clock_div <= 3'd0; 
        else
            clock_div <= phaseshift_output_clock[2:0];
    end
    else begin
        posbitclk <= ~posbitclk;
        if(clock_div == 4) begin
            blockclk <= 1; // ~block_clock;
            clock_div <= 0;
            block_clock_posedge <= 1; //~deser_block_clock;
        end
        else if(clock_div == 0) begin
            clock_div <= 1;
            block_clock_posedge <= 0;
            blockclk <= 0; //~block_clock;
        end
        else begin
            clock_div <= clock_div + 3'b1;
            block_clock_posedge <= 0;
        end
    end
end

always @(negedge localclock) begin
    if(~reset)
        negbitclk <= ~negbitclk;
end

/*
wire block_clock_w;
assign block_clock_w = blockclk ^ 0;
wire bit_clock_w;
assign bit_clock_w   = posbitclk ^ negbitclk;

BUFIO bbit(
    .I(bit_clock_w),
    .O(bit_clock)
);

BUFIO bblock(
    .I(block_clock_w),
    .O(block_clock)
);*/

assign bit_clock = posbitclk ^ negbitclk;
assign block_clock = blockclk ^ 0;

endmodule