/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:     KIT-ADL
// Engineer:    Rudolf Schimassek
// 
// Create Date: 05.09.2019
// Design Name: 
// Module Name: Data Decoder for untriggered Readout on ATLASPix3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
//
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module untriggered_decoder(
	input  wire clock,
	input  wire reset,
	//input  wire workonposedge,
	input  wire [9:0] datain,
	output reg  [8:0] dataout
);

parameter numdecoders = 4;
parameter addrwidth   = 2;

reg  [9:0] encoded[numdecoders-1:0];
wire [8:0] decoded[numdecoders-1:0];

reg [addrwidth-1:0] phase;

integer i;
always @(negedge clock) begin
    if(reset) begin
        phase <= 0;
        for(i = 0; i < numdecoders; i = i+1) begin
            encoded[i] <= 10'b0;
        end
    end
    else begin
        if(phase < numdecoders-1)
            phase <= phase + {addrwidth{1'b1}};
        else
            phase <= {addrwidth{1'b0}};
        
        dataout        <= decoded[phase];
        encoded[phase] <= datain;
    end
end

genvar j;
generate
    for(j = 0; j < numdecoders; j = j + 1) begin
        decode decoder(
            .datain(encoded[j]),
            .dispin(), 
            .dataout(decoded[j]),  
            .dispout(),
            .code_err(),
            .disp_err()
        ); 
    end
endgenerate

endmodule