/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
`define MASTER
 
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	KIT-ADL
// Engineer: 
// 
// Create Date: 06.08.2019 09:23:54
// Design Name: 
// Module Name: fastreadout
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fastreadout(
    input  wire        clock,
    input  wire        iv_deser_bit_clock,
    input  wire        iv_deser_bit_clock_half,
    input  wire        iv_deser_block_clock,
    input  wire        reset,
    input  wire        enable,
    input  wire        debug_output,
    input  wire        data_mux,

    input  wire [2:0]  deser_phaseshift_input_clock,        //to invert the clock
    input  wire [3:0]  deser_phaseshift_output_clock,
    input  wire        data_switch,
    
    input  wire        use_CkRef,
    input  wire        trigger_chip, //for fast readout configuration trigger signal
    //input  wire [7:0]  trigger_delay,
    //input  wire [7:0]  trigger_duration,
    input  wire [7:0]  tsdiv,
    input  wire [7:0]  ts2div,
    input  wire [7:0]  tsphase,
    
    input  wire        trigger_ref,     //trigger input for data taking
    input  wire [39:0] trigger_ts,
    input  wire [15:0] trigger_id,
    
    //data output to FIFO:
    output wire [63:0] FIFO_data_out,
    output wire        FIFO_wr_clock,
    output wire        FIFO_wr_enable,
    //input  wire        FIFO_full,
    
    //differential inputs and outputs:
    input  wire        ckref_chipout_p,
    input  wire        ckref_chipout_n,
    
      `ifdef MASTER
      
      
      output wire        ckref_p,
      output wire        ckref_n,
      output wire        sync_rst_p,
      output wire        sync_rst_n,
      
      
      `else
      
      input wire        ckref_p,
      input wire        ckref_n,
      input wire        sync_rst_p,
      input wire        sync_rst_n,
      output wire  ckref_input,     
      
      
      `endif
      
      
      

    output wire        ckext_p,
    output wire        ckext_n,

    output wire        ext_trigger_p,
    output wire        ext_trigger_n,
    input  wire        data_p,
    input  wire        data_n,
    //input  wire        data2_p,
    //input  wire        data2_n,
    
    //debug outputs:
    output wire        alignment_found,
    output wire        debug_empty_data,
    output wire        debug_data_start,
    output wire        debug_ckref_from_chip,
    output wire        sync_rst,
    output wire        ts_overflow,
    
    output wire        deser_bit_clock,
    output wire [8:0]  delayed_clock,
    
    input  wire        invertreceiverclock,
    output wire injtrigger,
    input wire fast_clk_40,
    input wire ck100
    );

wire ckref_out;
wire data;
wire data_sync;

wire ckref;
wire ext_trigger;
wire ckext;

assign ckref = ck100;//iv_deser_bit_clock_half & enable;//HACK 22 = ckext = 100MHz = sysclk 



assign ext_trigger = 0; //trigger_chip & trigger_ref;  //not used for untriggered Readout


assign ckext = iv_deser_bit_clock_half & enable;//Ivan


assign debug_ckref_from_chip = ckref_out;

// === data receiving ===
wire [9:0] deser_output;
wire [9:0] deser_output2;

//block clock generation
//wire deser_bit_clock;
wire deser_block_clock;
wire deser_block_clock_posedge;

/*
ByteClockGenerator bcgen(
    .clock(clock),
    .reset(reset),
    
    .phaseshift_input_clock(deser_phaseshift_input_clock),
    .phaseshift_output_clock(deser_phaseshift_output_clock),
    
    .bit_clock(deser_bit_clock),
    .block_clock(deser_block_clock),
    .block_clock_posedge(deser_block_clock_posedge),
    
    .delayed_clock(delayed_clock)
);

*/

wire [4:0] data_in_to_device;

//actual receiver IP:
untrigRO_selectIO deser(
    .data_in_from_pins_p(data_p),
    .data_in_from_pins_n(data_n),
    .data_in_to_device(data_in_to_device),
    .bitslip(0),
    //.clk_in((invertreceiverclock)?~deser_bit_clock : deser_bit_clock),
    //.clk_in(iv_deser_bit_clock),//Ivan
    .clk_in(iv_deser_bit_clock),//Ivan
    
    .clk_div_in(fast_clk_40),//Ivan
    .io_reset(0)
);




//IF msb is sent first, MSB will end up in the data (0)!


//assign inj_chopper = injection;

reg [4:0] shift_in0 , shift_in1;

wire [9:0] shift_in;


always @ (posedge fast_clk_40) begin

    shift_in0 <= {data_in_to_device[4], data_in_to_device[3],data_in_to_device[2], data_in_to_device[1], data_in_to_device[0]};
    
    shift_in1 <= shift_in0;

end

assign shift_in = {shift_in0,shift_in1};



/*

wire data;
reg [9:0] shiftdata;
reg [9:0] pardata;

IBUFDS #(
    .DIFF_TERM("TRUE"),
    .IBUF_DELAY_VALUE("0"),  
    .IOSTANDARD("LVDS_25")
) IBUFDS_datain (
    .I(data_p),
    .IB(data_n),
    .O(data)
);

wire shiftck;
//assign shiftck = (invertreceiverclock)? ~deser_bit_clock : deser_bit_clock;

always @(posedge deser_bit_clock) begin

    shiftdata[9:0] <= {data,shiftdata[9:1]};

end




always @(posedge iv_deser_block_clock) begin

    pardata <= shiftdata;

end
  
*/

//untrigRO_selectIO deser2(
//    .data_in_from_pins_p(data2_p),
//    .data_in_from_pins_n(data2_n),
//    .data_in_to_device(deser_output2),
//    .bitslip(0),
//    .clk_in((invertreceiverclock)?~deser_bit_clock : deser_bit_clock),
//    .clk_div_in(deser_block_clock),
//    .io_reset(0)
//);

// === data alignment ===
wire [9:0]  aligned_data;
untriggered_data_aligner align(
    .clock(clock),
    .reset(reset),
    
    .block_update(deser_block_clock_posedge),
    //.datain(deser_output),//Ivan
    
    //.datain(pardata),//Ivan
    
    .datain(shift_in),//Ivan
    
    .dataout(aligned_data),
    .alignment_found(alignment_found)
);


wire [8:0] decoded_data;
untriggered_decoder decode(
    .clock(iv_deser_block_clock),//Ivan
    .reset(reset),
    //.workonposedge(0),
    .datain(aligned_data[9:0]),
    .dataout(decoded_data[8:0])
); 

//time stamp generation:
wire [39:0] ts;
wire [31:0] ts2;
untriggered_TSGenerator tsgen(
    //.clock(clock), //deser_block_clock),
     `ifndef MASTER
    

    .sync_rst_input(sync_rst_input),     
    
    
    `endif

    
    .clock(iv_deser_bit_clock_half), //deser_block_clock),for fast count sync rst doesnt work
    .reset(reset),
    .enable(enable),
    
    .tsdiv(tsdiv),
    .ts2div(ts2div),
    .tsphase(tsphase),
    
    .tsout(ts),
    .ts2out(ts2),
    .syncReset(sync_rst),
    .overflowsync(ts_overflow),
    .injtrigger(injtrigger)
);

//data interpreter:
wire wr_en;
assign FIFO_wr_enable = wr_en & trigger_chip; //only write to FIFO if trigger is enabled
untriggered_statemachine SM(
    .clock(iv_deser_block_clock),
    .reset(reset),
    .enable(enable),
    .debug_output(debug_output),
    .data_mux(data_mux),
    
    .debug_data({aligned_data[9:0],decoded_data[8:0]}),
    .commaword(decoded_data[8]),
    .datain(decoded_data[7:0]),
    .ts_from_SM(ts),
    .ts2_from_SM(ts2),
    .trigger_ts(trigger_ts),
    .trigger_id(trigger_id),
    
    .dataset(FIFO_data_out[63:0]),
    .FIFO_wr_clk(FIFO_wr_clock),
    .FIFO_wr_enable(wr_en),
    
    .debug_state(),
    .debug_found_empty_output(debug_empty_data),
    .debug_found_data_start(debug_data_start)
);

//buffers for input and output:

IBUFDS #(
    .DIFF_TERM("FALSE"),
    .IBUF_DELAY_VALUE("0"),  
    .IOSTANDARD("LVDS_25")
) IBUFDS_ckref_out (
    .I(ckref_chipout_p),
    .IB(ckref_chipout_n),
    .O(ckref_out)
);








//IBUFDS #(
//    .DIFF_TERM("FALSE"),
//    .IBUF_DELAY_VALUE("0"),  
//    .IOSTANDARD("LVDS_25") //"DIFF_HSTL_I_12")
//) IBUFDS_data (    
//    .O(data),    
//    .I(data_p),   
//    .IB(data_n)
//);



//gtwizard_0 datainput(
//    .sysclk_in(clk640),
//    .soft_reset_rx_in(reset),
//    .gt0_drp_busy_out(),
    
//    .gt0_gtprxn_in(data_n),
//    .gt0_gtprxp_in(data_p)
//);



//assign obuf_i = {sync_rst & invertreceiverclock, ckref, ext_trigger, ckext};//Ivan HACK


//here

      `ifdef MASTER
      
      
      wire [3:0] obuf_i;
      wire [3:0] obuf_p;
      wire [3:0] obuf_n;
      
      
      assign obuf_i = {sync_rst, ckref, ext_trigger, ckext};//Ivan HACK
      
      
      
      assign obuf_p = {sync_rst_p, ckref_p, ext_trigger_p, ckext_p};
      assign obuf_n = {sync_rst_n, ckref_n, ext_trigger_n, ckext_n};
      
      genvar i;
      generate
          for(i = 0; i < 4; i = i + 1) begin
              OBUFDS #(
                  .IOSTANDARD("LVDS_25")
              ) OBUFDS_I (
                  .I(obuf_i[i]),
                  .O(obuf_p[i]),
                  .OB(obuf_n[i])
              );
          end
      endgenerate
      
      
      `else
      
      
      wire [1:0] obuf_i;
      wire [1:0] obuf_p;
      wire [1:0] obuf_n;
      
      assign obuf_i = {ext_trigger, ckext};//Ivan HACK     
      
      assign obuf_p = {ext_trigger_p, ckext_p};
      assign obuf_n = {ext_trigger_n, ckext_n};
      
      genvar i;
      generate
          for(i = 0; i < 2; i = i + 1) begin
              OBUFDS #(
                  .IOSTANDARD("LVDS_25")
              ) OBUFDS_I (
                  .I(obuf_i[i]),
                  .O(obuf_p[i]),
                  .OB(obuf_n[i])
              );
          end
      endgenerate
      
      wire sync_rst_input;      
      
      
      IBUFDS #(
          .DIFF_TERM("FALSE"),
          .IBUF_DELAY_VALUE("0"),  
          .IOSTANDARD("LVDS_25")
      ) IBUFDS_sync_rst_input (
          .I(sync_rst_p),
          .IB(sync_rst_n),
          .O(sync_rst_input)
      );
      
            //wire ckref_input;      
      
      
      IBUFDS #(
          .DIFF_TERM("FALSE"),
          .IBUF_DELAY_VALUE("0"),  
          .IOSTANDARD("LVDS_25")
      ) IBUFDS_ckref_input (
          .I(ckref_p),
          .IB(ckref_n),
          .O(ckref_input)
      );

      
      
      `endif
      
      
      






endmodule
