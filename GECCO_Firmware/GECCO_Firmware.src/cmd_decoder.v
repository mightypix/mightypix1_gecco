/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//////////////////////////////////////////////////////////////////////////////////
// Company:     KIT-ADL
// Engineer:    Rudolf Schimassek
// 
// Create Date: 14.11.2019
// Design Name: 
// Module Name: CMD Decoder Data Sender
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
//
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module cmd_decoder(
    input  wire       reset,
    input  wire       enable,
    input  wire       clock,
    
    input  wire [7:0] data,
    output reg        rd_clk,
    output reg        rd_en,
    input  wire       fifo_empty,
    
    output reg        cmd_line
);

reg [2:0] clockdivider = 0;
//these two signals form a "handshake":
reg       async_rd_en = 0;
reg       processed   = 0;
//the data sender toggles `async_rd_en` to mark the necessity of loading the next dataset
//  and the data reader toggles `processed` and checks the XOR of the signals for setting
//  the rd_en for the FIFO

reg [2:0] bitnumber = 3'b0;
wire [15:0] syncword;
assign syncword[15:0] = 16'b1000_0001_0111_1110;
reg syncpart = 0;
reg [7:0] senddata = 8'b0;

always @(posedge clock) begin
    if(reset) begin
        cmd_line <= 0;
        bitnumber <= 0;
        senddata <= syncword;
        syncpart <= 0;
        
        rd_en <= 0;
        rd_clk <= 0;
        clockdivider <= 0;
    end
    else begin
        clockdivider <= clockdivider + 3'b1;
        case(clockdivider)
            0: rd_clk <= 1;
            2: rd_clk <= 0;
            //rd_en synchronisation:
            3: begin
                if(async_rd_en ^ processed) begin
                    rd_en <= 1;
                    processed <= ~processed;
                end
                else if(rd_en)
                    rd_en <= 0;
            end
        endcase
        
    
        if(enable) begin
            bitnumber <= bitnumber + 3'b1;
            if(bitnumber == 7) begin
                syncpart <= ~syncpart;
                //always send sync word if no data is available
                if(fifo_empty) begin
                    senddata <= (syncpart)? syncword[15:8] : syncword[7:0];
                end
                else begin
                    senddata <= data;
                    async_rd_en <= ~async_rd_en;
                end
            end
            else
                senddata <= {1'b0, senddata[7:1]};
            cmd_line <= senddata[0];
        end
        else
            cmd_line <= 0;
    end
end


endmodule
