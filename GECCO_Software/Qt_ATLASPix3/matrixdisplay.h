#ifndef MATRIXDISPLAY_H
#define MATRIXDISPLAY_H

#include <QMainWindow>

#include "fastreadout.h"

namespace Ui {
class MatrixDisplay;
}

class MatrixDisplay : public QMainWindow
{
    Q_OBJECT

public:
    explicit MatrixDisplay(QWidget *parent = 0);
    ~MatrixDisplay();

    const int columns = 29;
    const int rows    = 12;
    const int pixelwidth = 165; //width in um
    const int pixelheight = 165; //height in um

    void AddHits(const std::map<Dataset, int> &hitdata);
    void AddHits(const std::vector<Dataset>& hitdata);
    void AddHits(const Dataset& hitdata);
    void ClearHits();

private slots:
    void on_B_Redraw_clicked();

    void on_B_Clear_clicked();

    void on_comboBox_plotType_currentIndexChanged(int index);

    void resizeEvent(QResizeEvent *event);

    void on_B_Close_clicked();

    void on_SB_ColourScaleMax_valueChanged(int arg1);

    void on_SB_LayerSelect_valueChanged(int arg1);

private:
    Ui::MatrixDisplay *ui;

    QImage image;
    QSize matrixsize;
    int nhits;

    int layers;

    std::vector<std::vector<unsigned int> > hitcollect;

    uint ToColour(double fraction);
    int FindImageWidth(int height);
    int FindImageHeight(int width);
    void ScaleImage();

    QPoint closepos;
    QPoint closesize;
};

#endif // MATRIXDISPLAY_H
