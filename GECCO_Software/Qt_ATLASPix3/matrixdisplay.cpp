#include "matrixdisplay.h"
#include "ui_matrixdisplay.h"

MatrixDisplay::MatrixDisplay(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MatrixDisplay), layers(0)
{
    ui->setupUi(this);

    matrixsize = QSize(columns, rows);

    std::vector<unsigned int> columncontent;
    columncontent.resize(rows, 0);
    hitcollect.clear();
    hitcollect.resize(columns, columncontent);

    image = QImage(matrixsize, QImage::Format_RGB32);
    image.fill(Qt::black);
    nhits = 0;
    ScaleImage();

    QRect geom = ui->B_Close->geometry();
    closepos.setX(geom.left());
    int h = geometry().height();
    closepos.setY(h - geom.top());
    closesize.setY(geom.height());

    on_B_Redraw_clicked();
}

MatrixDisplay::~MatrixDisplay()
{
    delete ui;
}

void MatrixDisplay::AddHits(const std::map<Dataset, int> &hitdata)
{
    double       maxvalue = 1. / ui->SB_ColourScaleMax->value();
    int          yoffset  = matrixsize.height() - 1;
    unsigned int c;

    if(ui->comboBox_plotType->currentIndex() == 1)
    {
        for(const auto& it : hitdata)
        {
            //only show the selected layer (or all for 0)
            //if(it.first.layer != layers && layers != 0)
                //continue;

            if(it.first.column >= 0 && it.first.column < columns
                    && it.first.row >= 0 && it.first.row < rows)
            {
                hitcollect[it.first.column][it.first.row] += it.second;
                nhits += it.second;
                c = ToColour(hitcollect[uint(it.first.column)][uint(it.first.row)] * maxvalue);
                image.setPixel(it.first.column, yoffset-it.first.row, c);
            }
        }
    }
    else
        std::cerr << "This display mode is not implemented" << std::endl;

    //draw the updated image:
    //static std::mutex imagelock;
    //if(!imagelock.try_lock())
    //    return false;
    QImage small = image.scaled(this->ui->L_Image->size());
    this->ui->L_Image->setPixmap(QPixmap::fromImage(small));
    //imagelock.unlock();
    //update hit count:
    ui->L_HitCount->setText(QString::number(nhits) + " Hits");
}

void MatrixDisplay::AddHits(const std::vector<Dataset> &hitdata)
{
    std::map<Dataset, int> sorteddata;

    for(const auto& it : hitdata)
    {
        auto sit = sorteddata.find(it);
        if(sit == sorteddata.end())
            sorteddata.insert(std::make_pair(it, 1));
        else
            ++(sit->second);
    }

    AddHits(sorteddata);
}

void MatrixDisplay::AddHits(const Dataset &hitdata)
{
    std::map<Dataset, int> data;
    data.insert(std::make_pair(hitdata, 1));
    AddHits(data);
}

void MatrixDisplay::ClearHits()
{
    on_B_Clear_clicked();
}

uint MatrixDisplay::ToColour(double fraction)
{
    //   0 -> blue   (  0,   0, 255)
    // 255 -> yellow (255, 255,   0)
    // RGB
    if(fraction > 1)
        fraction = 1;
    else if(fraction < 0)
        fraction = 0;

    //black if literally nothing:
    if(fraction < 1e-6)
        return 0;
    else
        return uint(255 * (1. - fraction)) + 65792 * uint(255 * fraction);
    //        blue                    |     red & green (256 * (256 + 1))
}

int MatrixDisplay::FindImageWidth(int height)
{
    return int(height * columns / double(rows) * pixelwidth / pixelheight);
}

int MatrixDisplay::FindImageHeight(int width)
{
    return int(width * rows / double(columns) * pixelheight / pixelwidth);
}

void MatrixDisplay::ScaleImage()
{
    QPoint pos = ui->L_Image->pos();
    int height = this->height() - 49 - pos.y();
    int width = FindImageWidth(height);
    if(width + pos.x() + 10 > this->width())
    {
        width = this->width() - pos.x() - 10;
        height = FindImageHeight(width);
    }
    ui->L_Image->setGeometry(pos.x(), pos.y(), width, height);
}

void MatrixDisplay::on_B_Redraw_clicked()
{
    double maxvalue = 1. / ui->SB_ColourScaleMax->value();
    int yoffset = matrixsize.height() - 1;
    unsigned int c = 0;
    for (int col = 0; col < matrixsize.width(); ++col)
    {
        for(int row = 0; row < matrixsize.height(); ++row)
        {
            switch(ui->comboBox_plotType->currentIndex())
            {
                case 0: //ToT
                    c = ToColour(hitcollect[uint(col)][uint(row)] * maxvalue);
                    break;
                case 1: //Counts
                    c = ToColour(hitcollect[uint(col)][uint(row)] * maxvalue);
                    break;
            default:
                    c = 0;
            }

            image.setPixel(col, yoffset-row, c);
        }
    }

    //draw the updated image:
    QImage small = image.scaled(this->ui->L_Image->size());
    this->ui->L_Image->setPixmap(QPixmap::fromImage(small));
    //update hit count:
    ui->L_HitCount->setText(QString::number(nhits) + " Hits");
}

void MatrixDisplay::on_B_Clear_clicked()
{
    image.fill(Qt::black);
    //clear data collector:
    for(auto& it : hitcollect)
        std::fill(it.begin(), it.end(), 0);
    nhits = 0;
    on_B_Redraw_clicked();
}

void MatrixDisplay::on_comboBox_plotType_currentIndexChanged(int index)
{
    (void) index;
    on_B_Clear_clicked();
}

void MatrixDisplay::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);

    ScaleImage();
    QRect geom = ui->B_Close->geometry();
    int h = geometry().height();
    geom.setTop(h - closepos.y());
    geom.setHeight(closesize.y());
    ui->B_Close->setGeometry(geom);
}

void MatrixDisplay::on_B_Close_clicked()
{
   close();
}

void MatrixDisplay::on_SB_ColourScaleMax_valueChanged(int arg1)
{
    (void) arg1;
    on_B_Redraw_clicked();
}

void MatrixDisplay::on_SB_LayerSelect_valueChanged(int arg1)
{
    layers = arg1;
}
