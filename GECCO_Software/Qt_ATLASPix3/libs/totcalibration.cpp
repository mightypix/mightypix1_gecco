/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "totcalibration.h"

PointCurve::PointCurve() :  points(std::vector<tuple>()), maximum(0),
            threshold(-1), noise(-1),
            windowstart(-1), windowend(-1), windownoise(-1)
{

}

void PointCurve::SetMaximum(double max)
{
    if(max >= 0)
        maximum = max;
}

double PointCurve::GetMaximum()
{
    return maximum;
}

void PointCurve::AddPoint(double voltage, double hits, double error)
{
    //if(hits > maximum)
    //    maximum = hits;
    points.push_back(tuple(voltage, hits, error));
}

int PointCurve::AddHits(double voltage, double hits)
{
    bool done = false;
    int entries = 0;
    for(auto& it : points)
    {
        if(std::abs(it.x - voltage) < 1e-5)
        {
            done = true;
            it.y += hits;
            entries = int(it.y);
        }
    }
    if(!done)
    {
        AddPoint(voltage, hits);
        entries = hits;
    }

    return entries;
}

void PointCurve::ClearPoints()
{
    maximum = 0;
    threshold = -1;
    noise = -1;

    points.clear();
}

unsigned int PointCurve::GetNumPoints()
{
    return points.size();
}

std::pair<double, double> PointCurve::GetPoint(unsigned int index)
{
    if(index < points.size())
        return std::make_pair(points[index].x, points[index].y);
    else
        return std::make_pair(double(-1e10), double(0));
}

std::pair<double, double> PointCurve::GetNormedPoint(unsigned int index)
{
    if(index < points.size())
    {
        std::pair<double, double> pt;
        pt.first  = points[index].x;
        pt.second = points[index].y/double(maximum);
        return pt;
    }
    else
        return std::make_pair(double(-1e10), double(0));
}

double PointCurve::GetPointError(unsigned int index)
{
    if(index < points.size())
        return points[index].yerr;
    else
        return -1e10;
}

void PointCurve::SetPointError(unsigned int index, double error)
{
    if(index < points.size())
        points[index].yerr = error;
}

double PointCurve::GetPointVoltage(unsigned int index)
{
    if(index < points.size())
        return points[index].x;
    else
        return -1e10;
}

unsigned int PointCurve::GetPointSignals(unsigned int index)
{
    if(index < points.size())
        return points[index].y;
    else
        return 0;
}

int PointCurve::GetSignalsToVolt(double voltage)
{
    for(auto& it : points)
        if(std::abs(it.x - voltage) < 0.001)
            return int(it.y);

    return -1;
}

double PointCurve::GetPointNormedSignals(unsigned int index)
{
    if(index < points.size())
        return points[index].y / double(maximum);
    else
        return 0;
}

double PointCurve::GetIntegral()
{
    double integral = 0;
    for(auto& it : points)
        integral += it.y;

    return integral;
}

double PointCurve::GetThreshold()
{
    return threshold;
}

void PointCurve::SetThreshold(double thr)
{
    threshold = thr;
}

double PointCurve::GetNoise()
{
    return noise;
}

void PointCurve::SetNoise(double noise)
{
    this->noise = noise;
}

double PointCurve::GetWindowStart()
{
    return windowstart;
}

void PointCurve::SetWindowStart(double start)
{
    windowstart = start;
}

double PointCurve::GetWindowEnd()
{
    return windowend;
}

void PointCurve::SetWindowEnd(double end)
{
    windowend = end;
}

double PointCurve::GetWindowNoise()
{
    return windownoise;
}

void PointCurve::SetWindowNoise(double noise)
{
    windownoise = noise;
}

std::map<double, double> PointCurve::GetNormedSortedMap()
{
    std::map<double, double> data;

    for(auto& it : points)
        data.insert(std::make_pair(it.x,it.y/double(maximum)));

    return data;
}

std::string PointCurve::GenerateString(std::string title, bool normed)
{
    std::stringstream s("");

    //header lines:
    s << "# PointCurve: " << title << std::endl;
    if(std::abs(threshold - (-1)) > 1e-5 || std::abs(noise - (-1)) > 1e-5)
        s << "# Threshold: " << threshold << std::endl
          << "# Noise:     " << noise << std::endl;
    else if(std::abs(windowstart - (-1)) > 1e-5 || std::abs(windowend - (-1)) > 1e-5 || std::abs(windownoise - (-1)) > 1e-5)
        s << "# Window Start: " << windowstart << std::endl
          << "# Window End:   " << windowend << std::endl
          << "# Noise:        " << windownoise << std::endl;

    //data lines (including legend):
    if(normed)
    {
        s << "# inj. Voltage (in V); Fraction of Signals detected; Error" << std::endl;
        for(auto& it : points)
            s << it.x << "\t" << it.y/double(maximum) << "\t" << it.yerr / double(maximum) << std::endl;
    }
    else
    {
        s << "# inj. Voltage (in V); Number of Signals detected (of " << maximum << "); Error" << std::endl;
        for(auto& it : points)
            s << it.x << "\t" << it.y << "\t" << it.yerr << std::endl;
    }

    return s.str();
}

double PointCurve::EvalSCurve(double x, double x0, double width)
{
    return (1. + erf((x-x0) / sqrt(2) / width)) * 0.5; // / 2.;
}

double PointCurve::EvalWindow(double x, double x0, double x1, double width)
{
    return (1. + erf((x - x0) / sqrt(2) / width)) * (1. + erf((x1 - x) / sqrt(2) / width)) * 0.25;
}

bool smaller(tuple lhs, tuple rhs)
{
    return lhs.x < rhs.x;
}

void PointCurve::Sort()
{
    std::sort(points.begin(), points.end(), smaller);
}


ToTCalibration::ToTCalibration(const Configuration& config) : interval_start(0), interval_step(1),
    interval_end(255), numsignals(100)
{
    this->config = config;
}

Configuration *ToTCalibration::GetConfig()
{
    return &config;
}

void ToTCalibration::SetConfig(const Configuration &config)
{
    this->config = config;
}

int ToTCalibration::GetInterval_start() const
{
    return interval_start;
}

void ToTCalibration::SetInterval_start(int value)
{
    interval_start = value;
}

void ToTCalibration::SetMeasType(int value, bool value2)
{
    MeasType = value;
    injordel = value2;
}


int ToTCalibration::GetInterval_step() const
{
    return interval_step;
}

void ToTCalibration::SetInterval_step(int value)
{
    interval_step = value;
}

int ToTCalibration::GetInterval_end() const
{
    return interval_end;
}

void ToTCalibration::SetInterval_end(int value)
{
    interval_end = value;
}

int ToTCalibration::GetNumSignals() const
{
    return numsignals;
}

void ToTCalibration::SetNumSignals(int value)
{
    numsignals = value;
}

int ToTCalibration::GetTSdiv() const
{
    return tsdiv;
}

void ToTCalibration::SetTSdiv(int value)
{
    tsdiv = value;
}

int ToTCalibration::GetTS2div() const
{
    return ts2div;
}

void ToTCalibration::SetTS2div(int value)
{
    ts2div = value;
}

void ToTCalibration::ConfigureInjections(int row, bool flush, int startcol, int endcol)
{
    if(row < 0 || row >= AP3rows)
        return;


    //ASIC_Config2* rowconfig = config.GetATLASPixConfig(Configuration::row);
    ASIC_Config2* colconfig = config.GetATLASPixConfig(Configuration::dac);

    //injections only to passed row:

    for(int i = 0; i < AP3rows; ++i)
    {
        std::stringstream sr("");
        sr << "en_injection_row_" << i;

        colconfig->SetParameter(sr.str(), (i == row)?1:0);
    }


    //injection to all pixels in the row:
    for(int i = 0; i < AP3columns; ++i)
    {
        std::stringstream sc("");
        sc << "en_injection_col_" << i;

        if(i >= startcol && i <= endcol)
            colconfig->SetParameter(sc.str(), 1);
        else
            colconfig->SetParameter(sc.str(), 0);

        //std::stringstream sc2("");
        //sc2 << "en_hitbus_col_" << i;
        //colconfig->SetParameter(sc2.str(), 1);

    }

    if(flush)
        config.SendUpdate(Configuration::dac);
}

std::map<Dataset, PointCurve> ToTCalibration::MeasureToT(bool *running, bool updategui,
                                                         Rect activepixels,
                                                         std::string histfilename)
{
    if(running == nullptr || *running == false)
        return std::map<Dataset, PointCurve>();

    bool debug = histfilename != "";

    //backup injection settings:
    Injection_Config* injconf_backup = config.GetInjectionConfig();
    if(injconf_backup == nullptr)
    {
        std::cerr << "Error: Injection config empty, aborting measurement." << std::endl;
        return std::map<Dataset,PointCurve>();
    }
    Injection_Config injconf = *injconf_backup; //create a working copy
    config.SetInjectionConfig(&injconf);
    injconf.SetNumPulseSets(static_cast<unsigned int>(numsignals));
    injconf.SetNumPulsesInaSet(static_cast<unsigned int>(1));




    /*

    //backup VDAC settings:
    ASIC_Config2* vdac_backup = config.GetATLASPixConfig(Configuration::vdac);
    if(vdac_backup == nullptr)
    {
        std::cerr << "Error: VDAC config empty, aborting measurement." << std::endl;
        return std::map<Dataset, PointCurve>();
    }
    ASIC_Config2 vdac = *vdac_backup; //create a working copy
    config.SetATLASPixConfig(Configuration::vdac, &vdac);

    */

    //ToT-Curve container:
    std::map<Dataset, PointCurve> pointcurves;

    //prepare the injections
    int splitinjections = 1;
    int numsignals_backup = numsignals;
    const int maxnuminjections = 100;
    if(injconf.GetNumPulseSets() > maxnuminjections)
    {
        splitinjections = injconf.GetNumPulseSets() / maxnuminjections + 1;
        injconf.SetNumPulseSets(maxnuminjections);
        //numsignals = maxnuminjections * splitinjections;
    }
    config.StopInjections(false);
    config.ConfigureInjections(false);

    //fitting statistics:
    int goodfits  = 0;
    int totalfits = 0;

    PointCurve dummy;

    //Measurement Loop:
    for(int i = interval_start; i <= interval_end && *running; i += interval_step)
    {
        //set injection voltage:


        //vdac.SetParameter("inject", static_cast<unsigned int>(i));
        //config.SendUpdate(Configuration::vdac);

        //!!!
        //!
        if(injordel){
            injconf.SetDAC("Out1", static_cast<double>(i*0.001));
            config.ConfigureInjections(true);

            std::stringstream s("");
            s << " Set DAC to : " << i*0.001;
            std::cout << s.str() << std::endl;
        }
        else{

            injconf.SetInitDelay(i);
            config.ConfigureInjections(false);

        }


        std::cout << "." << std::flush;

        //config.StopFastReadout(false);

        config.ResetFastReadout(-1, true);
        //config.GetNexysIO()->Write(0, NexysIO::Ld, false, 500);
                    //just as delay between reset and injection start

        //config.StartFastReadout(true,false);

        //Timing::Sleep(100);//here tot 2022
        //Timing::Sleep(300);

        std::vector<Dataset> hitlist;

        //set number of pulses per go to maximum if total number is bigger than
        //  maximum:
        if(splitinjections > 1)
        {
            injconf.SetNumPulseSets(maxnuminjections);
            config.ConfigureInjections(true);
        }
        //else not missing as for total number < maximum, no change in number is necessary

        for(int i = 0; i < splitinjections; ++i)
        {
            //do the remaining injections on last subset:

            if(i + 1 == splitinjections && splitinjections > 1)
            {
                injconf.SetNumPulseSets(uint(numsignals_backup - maxnuminjections * i));

                if(injconf.GetNumPulseSets() == 0)
                    break;

                config.ConfigureInjections(true);
            }


            Timing::Sleep(100);//2022

            config.RestartInjections(true);


            //config.StopInjections(true);
            Timing::Sleep(10);//2022
            //config.StartInjections(true);
            //Timing::Sleep(300);

            //read the data from the FPGA:
            std::vector<Dataset> hits = config.ReadoutAll(nullptr, false,
                                                          static_cast<unsigned int>(numsignals) * 5,
                                                          5, 5, 100);//max empty treads maden it slow!!! was 500

            //std::vector<Dataset> hits = config.ReadoutAll(nullptr, false, static_cast<unsigned int>(numsignals) * 5,
                                                          //((i < splitinjections - 1)?2:5), 5,
                                                          //maxtimeperpoint);

            //std::vector<Dataset> Configuration::ReadoutAll(bool *running, bool print, unsigned int maxnumhits,
                                                           //int maxemptyreads, int readsize, int maxtime)

            hitlist.insert(hitlist.end(), hits.begin(), hits.end());
        }

        //delete all hits from outside active area:
        std::vector<Dataset> newlist;
        bool first = true;
        for(auto& it : hitlist)
        {
            if(injordel) {if(it.column >= activepixels.startcol && it.column <= activepixels.endcol
                && it.row >= activepixels.startrow && it.row <= activepixels.endrow && it.time <= 400)
                if(!first) newlist.push_back(it);}
            else {if(it.column >= activepixels.startcol && it.column <= activepixels.endcol
                     && it.row >= activepixels.startrow && it.row <= activepixels.endrow)
                     if(!first) newlist.push_back(it);}
                first = false;
        }
        //sort the remaining hits into histograms:
        std::map<Dataset, Histogram> newdata = SortInHistogram(newlist);


        std::string voltage;

        if(injordel){
            std::stringstream svolt("");
            svolt << i*0.001;
            voltage = svolt.str();
        }
        else{

            std::stringstream svolt("");
            svolt << i;
            voltage = svolt.str();

        }



        //fit the histograms and add the resulting parameters (mean/sigma) to the point curves:
        for(auto& it : newdata)
        {
            //add missing signals as length "0":
            if(it.second.getIntegral() < numsignals)
                it.second.Fill(0, numsignals - it.second.getIntegral());

            if(Trimming::FitGaus(it.second, false))
            {
                ++goodfits;

                //Write histogram contents to a dedicated file:
                if(debug)
                {
                    it.second.setName(it.first.ToAddressString()+ " at " + voltage + " V");
                    WriteToFile(histfilename, it.second.Print());
                }

                //add the point to a pointcurve for the respective pixel:
                bool done = false;
                //search for the pointcurve of the pixel:
                for(auto& pcit : pointcurves)
                {
                    if(pcit.first == it.first)
                    {
                        if(injordel) pcit.second.AddPoint(i*0.001, it.second.GetMean(), it.second.GetSigma());
                        else pcit.second.AddPoint(i, it.second.GetMean(), it.second.GetSigma());
                        done = true;
                    }
                }
                //create a new curve if non exists already
                if(!done)
                {
                    PointCurve curve = dummy;
                    if(injordel) curve.AddPoint(i*0.001, it.second.GetMean(), it.second.GetSigma());
                    else curve.AddPoint(i, it.second.GetMean(), it.second.GetSigma());
                    pointcurves.insert(std::make_pair(it.first, curve));
                }
            }
        }
        totalfits += newdata.size();

        config.ProcessEvents();

        if(injordel) dummy.AddPoint(i*0.001, 0, 0);
        else dummy.AddPoint(i, 0, 0);
    }
    std::cout << "\n  fitting efficiency: " << goodfits / double(totalfits) << std::endl;

    //restore the back-uped configurations:
    config.SetInjectionConfig(injconf_backup);
    injconf_backup = nullptr;
    //config.SetATLASPixConfig(Configuration::vdac, vdac_backup);
    //vdac_backup = nullptr;

    numsignals = numsignals_backup;

    return pointcurves;
}

std::map<Dataset, PointCurve> ToTCalibration::MeasureToTArea(bool *running, int pixelsatonce,
                                                             Rect area, std::string filename,
                                                             std::string histfilename, bool disable)
{
    if(running == nullptr || *running == false)
        return std::map<Dataset, PointCurve>();

    std::map<Dataset, PointCurve> result;

    int lastcol = (area.endcol > AP3columns)?AP3columns - 1:area.endcol;

    //new
    if(disable){
    for(unsigned int disablerow = 0; disablerow < AP3rows; ++disablerow)
        for(unsigned int disablecol = 0; disablecol < AP3columns; ++disablecol)
            config.GetTDACConfig()->SetTDACValue(disablecol, disablerow, config.GetTDACConfig()->GetTDACValue(disablecol, disablerow) & 0b0111);

    config.WriteRAMMatrix();
    }

    /*
    for(unsigned int disablerow = 0; disablerow < area.startrow; ++disablerow)
        for(unsigned int disablecol = 0; disablecol < AP3columns; ++disablecol)
            config.GetTDACConfig()->SetTDACValue(disablecol, disablerow, config.GetTDACConfig()->GetTDACValue(disablecol, disablerow) & 0b0111);

    for(unsigned int disablerow = area.endrow + 1; disablerow < AP3rows; ++disablerow)
        for(unsigned int disablecol = 0; disablecol < AP3columns; ++disablecol)
            config.GetTDACConfig()->SetTDACValue(disablecol, disablerow, config.GetTDACConfig()->GetTDACValue(disablecol, disablerow) & 0b0111);

    config.WriteRAMMatrix();
*/
    int incrow;
    #ifdef CLICFast
    incrow = 1;
    #else
    incrow = 2;
    #endif

    for(int row = area.startrow; row <= area.endrow && *running; row = row + incrow)
    {
        std::cout << "Starting Row " << row << " ..." << std::endl;



        if(disable){
            for(unsigned int disablecol = 0; disablecol < AP3columns; ++disablecol)
            {

                if(row > 0) config.GetTDACConfig()->SetTDACValue(disablecol, row - 1, config.GetTDACConfig()->GetTDACValue(disablecol, row - 1) & 0b0111);

            }
            if(row > 0) config.WriteRAMRow(int(row) - 1);
        }



        Rect measurearea = area;
        measurearea.startrow = row;
        measurearea.endrow   = row;
        for(int col = area.startcol; col <= area.endcol && * running; col += 1)
        {
            measurearea.startcol = col;
            measurearea.endcol   = col;
            //if(measurearea.endcol > lastcol)
                //measurearea.endcol = lastcol;
            //std::cout << "   columns " << measurearea.startcol << " - " << measurearea.endcol
                      //<< " ..." << std::endl;


            if(disable){
                for(unsigned int disablecol = 0; disablecol < AP3columns; ++disablecol)
                {

                    config.GetTDACConfig()->SetTDACValue(disablecol, row, config.GetTDACConfig()->GetTDACValue(disablecol, row) & 0b0111);

                }

                config.GetTDACConfig()->SetTDACValue(col, row, config.GetTDACConfig()->GetTDACValue(col, row) | 0b1000);


                config.WriteRAMRow(int(row));

            }





            #ifdef CLICFast
            ConfigureInjections(row, true, col, col);
            #else
            ConfigureInjections((row/2)*2, true, col, col);
            #endif



            auto partresult = MeasureToT(running, false, measurearea, histfilename);

            result.insert(partresult.begin(), partresult.end());

            //save the data during the measurement:
            if(filename != "")
            {
                for(auto& it : partresult)
                    WriteToFile(filename, it.second.GenerateString(it.first.ToAddressString(),
                                                                   false) + "\n");
            }

            config.ProcessEvents();
        }
    }
    if(*running)
        std::cout << "finished Area " << area.ToString() << std::endl;

    if(disable){
    for(unsigned int disablerow = 0; disablerow < AP3rows; ++disablerow)
        for(unsigned int disablecol = 0; disablecol < AP3columns; ++disablecol)
            config.GetTDACConfig()->SetTDACValue(disablecol, disablerow, config.GetTDACConfig()->GetTDACValue(disablecol, disablerow) | 0b1000);

    config.WriteRAMMatrix();
    }

    return result;
}

std::map<Dataset, Histogram> ToTCalibration::SortInHistogram(std::vector<Dataset> &data)
{
    if(data.size() == 0)
        return std::map<Dataset, Histogram>();

    std::map<Dataset, Histogram> container;

    for(auto& it : data)
    {
        bool done = false;
        for(auto& hist : container)
        {
            if(hist.first == it)
            {
                done = true;

                //if(it.GetToTValue(tsdiv, ts2div) != 0) hist.second.Fill(it.GetToTValue(tsdiv, ts2div));
                //Ivan
                if(MeasType == 0) hist.second.Fill(it.tot);
                else if(MeasType == 1) hist.second.Fill(it.ts3);
                else if(MeasType == 2) hist.second.Fill(it.ts11);
                else if(MeasType == 3) hist.second.Fill(it.ts12);
                else if(MeasType == 4) hist.second.Fill(it.time);
            }
        }
        if(!done)
        {
            Histogram hist;
            /*
            int div = (tsdiv<ts2div)?tsdiv:ts2div;
            int numbins = 1024;
            if(128 * ts2div < 1024 * tsdiv)
                numbins = 128;
            */


            hist.addBins(10000);//Ivan
            hist.setBinWidth(0.1);//Ivan
            hist.setFirstBinMean(0.);
            hist.setOrientation(true);


/*
            if(MeasType == 0){//tot
                hist.addBins(2000);//Ivan
                hist.setBinWidth(0.5);//Ivan
                hist.setFirstBinMean(0.);
                hist.setOrientation(true);
            }
            else if(MeasType > 0) {
                hist.addBins(500);//Ivan
                hist.setBinWidth(0.25);//Ivan
                hist.setFirstBinMean(0.);
                hist.setOrientation(true);
            }
*/

            if(it.GetToTValue(tsdiv, ts2div) != 0) hist.Fill(it.GetToTValue(tsdiv, ts2div));

            container.insert(std::make_pair(it, hist));
        }
    }

    return container;
}

bool ToTCalibration::WriteToFile(std::string filename, std::string data)
{
    std::fstream f;
    f.open(filename.c_str(), std::ios::out | std::ios::app | std::ios::binary);

    if(!f.is_open())
        return false;

    f << data << std::flush;
    f.close();

    return true;
}

