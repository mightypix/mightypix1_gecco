/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "configuration.h"

Configuration::Configuration() : nexys(nullptr), atlaspix_config(nullptr),
    atlaspix_dac(nullptr), atlaspix_vdac(nullptr), atlaspix_tdac(nullptr), atlaspix_row(nullptr),
    atlaspix_column(nullptr), atlaspix_sr_loads(nullptr), tdacs(nullptr), injection(nullptr),
    fastro(nullptr), fastro_lastread(""), fm_resets(nullptr), fm_configmode(nullptr),
    fm_workmode(nullptr), spiconfig(nullptr), cmdc(nullptr), progressbarblocked(false),
    waiter(nullptr)
{
    configmeans = new int(1);
    ownconfigmeans = true;
    voltageboards[0] = nullptr;
    voltageboards[1] = nullptr;
#ifdef _useQT_
    progress = nullptr;
    log      = nullptr;
#endif
}

Configuration::Configuration(const Configuration &ref)
{
    configmeans            = ref.configmeans;
    ownconfigmeans         = false;
    nexys                  = ref.nexys;
    atlaspix_config        = ref.atlaspix_config;
    atlaspix_dac           = ref.atlaspix_dac;
    atlaspix_vdac          = ref.atlaspix_vdac;
    atlaspix_tdac          = ref.atlaspix_tdac;
    atlaspix_column        = ref.atlaspix_column;
    atlaspix_row           = ref.atlaspix_row;

    atlaspix_sr_loads      = ref.atlaspix_sr_loads;
    tdacs                  = ref.tdacs;

    injection              = ref.injection;
    fastro                 = ref.fastro;
    fastro_lastread        = ref.fastro_lastread;
    fastro_clockshifts     = ref.fastro_clockshifts;
    fastro_clockspeed      = ref.fastro_clockspeed;
    fastro_triggersettings = ref.fastro_triggersettings;

    fm_resets              = ref.fm_resets;
    fm_configmode          = ref.fm_configmode;
    fm_workmode            = ref.fm_workmode;

    spiconfig              = ref.spiconfig;
    cmdc                   = ref.cmdc;

    voltageboards[0]       = ref.voltageboards[0];
    voltageboards[1]       = ref.voltageboards[1];

    progressbarblocked     = ref.progressbarblocked;
#ifdef _useQT_
    progress               = ref.progress;
    log                    = ref.log;
#endif

    waiter                 = ref.waiter;
}

Configuration::~Configuration()
{
    if(ownconfigmeans)
        delete configmeans;
    configmeans            = nullptr;
    nexys                  = nullptr;
    atlaspix_config        = nullptr;
    atlaspix_dac           = nullptr;
    atlaspix_vdac          = nullptr;
    atlaspix_tdac          = nullptr;
    atlaspix_row           = nullptr;
    atlaspix_column        = nullptr;

    atlaspix_sr_loads      = nullptr;
    tdacs                  = nullptr;

    voltageboards[0]       = nullptr;
    voltageboards[1]       = nullptr;
    injection              = nullptr;

    fastro                 = nullptr;
    //fastro_lastread        = "";
    fastro_clockshifts     = nullptr;
    fastro_clockspeed      = nullptr;
    fastro_triggersettings = nullptr;

    fm_resets              = nullptr;
    fm_configmode          = nullptr;
    fm_workmode            = nullptr;

    spiconfig              = nullptr;
    cmdc                   = nullptr;

    waiter                 = nullptr;


#ifdef _useQT_
    progress               = nullptr;
    log                    = nullptr;
#endif
}

Configuration Configuration::operator=(const Configuration &ref)
{
    if(ownconfigmeans)
        delete configmeans;
    configmeans            = ref.configmeans;
    ownconfigmeans         = false;
    nexys                  = ref.nexys;
    atlaspix_config        = ref.atlaspix_config;
    atlaspix_dac           = ref.atlaspix_dac;
    atlaspix_vdac          = ref.atlaspix_vdac;
    atlaspix_tdac          = ref.atlaspix_tdac;
    atlaspix_column        = ref.atlaspix_column;
    atlaspix_row           = ref.atlaspix_row;

    atlaspix_sr_loads      = ref.atlaspix_sr_loads;
    tdacs                  = ref.tdacs;

    injection              = ref.injection;
    fastro                 = ref.fastro;
    fastro_lastread        = ref.fastro_lastread;
    fastro_clockshifts     = ref.fastro_clockshifts;
    fastro_clockspeed      = ref.fastro_clockspeed;
    fastro_triggersettings = ref.fastro_triggersettings;

    fm_resets              = ref.fm_resets;
    fm_configmode          = ref.fm_configmode;
    fm_workmode            = ref.fm_workmode;

    spiconfig              = ref.spiconfig;
    cmdc                   = ref.cmdc;

    voltageboards[0]       = ref.voltageboards[0];
    voltageboards[1]       = ref.voltageboards[1];

    progressbarblocked     = ref.progressbarblocked;
#ifdef _useQT_
    progress               = ref.progress;
    log                    = ref.log;
#endif

    waiter                 = ref.waiter;

    return ref;
}

void Configuration::setWaiter(WaitFunction newwaiter)
{
    this->waiter = newwaiter;
}

void Configuration::ProcessEvents()
{
    if(waiter != nullptr)
        waiter();
}

std::string Configuration::FindFileName(std::string filenameprefix, std::string filenamesuffix)
{
    std::fstream f;
    int fileindex = 0;
    do{
        f.close();
        std::stringstream s("");
        s << filenameprefix << ++fileindex << filenamesuffix;
        f.open(s.str().c_str(), std::ios::in);
    }while(f.is_open());

    std::stringstream s("");
    s << filenameprefix << fileindex << filenamesuffix;

    return s.str();
}

std::string Configuration::WriteToFile(std::string filename, std::string data)
{
    std::fstream f;
    f.open(filename.c_str(), std::ios::out | std::ios::app | std::ios::binary);
    if(!f.is_open())
        return "";

    f << data;
    f.flush();
    f.close();

    return filename;
}


std::string Configuration::WriteToFile(std::string filenameprefix, std::string filenamesuffix, std::string data)
{
    std::fstream f;
    std::string filename = FindFileName(filenameprefix, filenamesuffix);
    f.open(filename.c_str(), std::ios::out | std::ios::app | std::ios::binary);

    f << data;
    f.flush();
    f.close();

    return filename;
}

NexysIO* Configuration::GetNexysIO() const
{
    return nexys;
}

void Configuration::SetNexysIO(NexysIO* value)
{
    nexys = value;
}

ASIC_Config2* Configuration::GetATLASPixConfig(int flag)
{
    switch (flag)
    {
    case(dac):
        return atlaspix_dac;
    case(config):
        return atlaspix_config;
    case(vdac):
        return atlaspix_vdac;
    case(column):
        return atlaspix_column;
    case(row):
        return atlaspix_row;
    case(tdac):
        return atlaspix_tdac;
    case(srload):
        return atlaspix_sr_loads;
    default:
        return nullptr;
    }
}

bool Configuration::SetATLASPixConfig(int flag, ASIC_Config2 *config)
{
    switch (flag)
    {
    case(dac):
        atlaspix_dac = config;
        return true;
    case(configs::config):
        atlaspix_config = config;
        return true;
    case(vdac):
        atlaspix_vdac = config;
        return true;
    case(column):
        atlaspix_column = config;
        return true;
    case(row):
        atlaspix_row = config;
        return true;
    case(tdac):
        atlaspix_tdac = config;
        return true;
    case(srload):
        atlaspix_sr_loads = config;
        return true;
    default:
        return false;
    }
}

TDAC_Config *Configuration::GetTDACConfig()
{
    return tdacs;
}

void Configuration::SetTDACConfig(TDAC_Config *config)
{
    tdacs = config;
}

VB_Config *Configuration::GetVBConfig(int index)
{
    switch(index)
    {
    case(0):
        return voltageboards[0];
    case(1):
        return voltageboards[1];
    default:
        return nullptr;
    }
}

bool Configuration::SetVBConfig(int index, VB_Config *config)
{
    switch(index)
    {
    case(0):
        voltageboards[0] = config;
        return true;
    case(1):
        voltageboards[1] = config;
        return true;
    default:
        return false;
    }
}

Injection_Config *Configuration::GetInjectionConfig()
{
    return injection;
}

void Configuration::SetInjectionConfig(Injection_Config *config)
{
    injection = config;
}

FastReadout *Configuration::GetFastReadoutConfig()
{
    return fastro;
}

void Configuration::SetFastReadoutConfig(FastReadout *config)
{
    fastro = config;
}

FlagManager *Configuration::GetFastReadoutClockShiftConfig()
{
    return fastro_clockshifts;
}

void Configuration::SetFastReadoutClockShiftConfig(FlagManager *config)
{
    fastro_clockshifts = config;
}

FlagManager *Configuration::GetFastReadoutClockSpeedConfig()
{
    return fastro_clockspeed;
}

void Configuration::SetFastReadoutClockSpeedConfig(FlagManager *config)
{
    fastro_clockspeed = config;
}

FlagManager *Configuration::GetFastReadoutTriggerSettingConfig()
{
    return fastro_triggersettings;
}

void Configuration::SetFastReadoutTriggerSettingConfig(FlagManager *config)
{
    fastro_triggersettings = config;
}

FlagManager *Configuration::GetPinConfig(int flag)
{
    switch(flag)
    {
    case(resets):
        return fm_resets;
    case(modeconfig):
        return fm_configmode;
    case(modework):
        return fm_workmode;
    default:
        return nullptr;
    }
}

bool Configuration::SetPinConfig(int flag, FlagManager *config)
{
    switch(flag)
    {
    case(resets):
        fm_resets = config;
        return true;
    case(modeconfig):
        fm_configmode = config;
        return true;
    case(modework):
        fm_workmode = config;
        return true;
    default:
        return false;
    }
}

SPINexys *Configuration::GetSPIConfig()
{
    return spiconfig;
}

void Configuration::SetSPIConfig(SPINexys *config)
{
    spiconfig = config;
}

CMDConfig *Configuration::GetCMDConfig()
{
    return cmdc;
}

void Configuration::SetCMDConfig(CMDConfig *config)
{
    cmdc = config;
}

#ifdef _useQT_
QProgressBar *Configuration::GetProgressBar()
{
    return progress;
}

void Configuration::SetProgressBar(QProgressBar *pb)
{
    progress = pb;
}

QTextEdit *Configuration::GetLogWindow()
{
    return log;
}

void Configuration::SetLogWindow(QTextEdit *log)
{
    this->log = log;
}
#endif

int Configuration::GetProgressBarValue()
{
#ifdef _useQT_
    if(progress != nullptr)
        return progress->value();
    else
        return 0;
#else
    return 0;
#endif
}

void Configuration::SetProgressBarValue(int value)
{
#ifdef _useQT_
    if(progress != nullptr)
        progress->setValue(value);
#else
    (void) value;
#endif
}

void Configuration::SetProgressBarMaximum(int value)
{
#ifdef _useQT_
    if(progress != nullptr)
        progress->setMaximum(value);
#else
    (void) value;
#endif
}

bool Configuration::GetProgressBarBlocked()
{
    return progressbarblocked;
}

bool Configuration::BlockProgressBar()
{
    if(progressbarblocked)
        return false;
    else
    {
        progressbarblocked = true;
        return true;
    }
}

bool Configuration::ReleaseProgressBar()
{
    if(!progressbarblocked)
        return false;
    else
    {
        progressbarblocked = false;
        return true;
    }
}

void Configuration::logit(std::string text, std::string header, bool error)
{
    if(error)
        std::cerr << text << std::endl;
    else
        std::cout << text << std::endl;

#ifdef _useQT_
    if(log != nullptr)
    {
        static std::string localheader = "";

        QString buffer= QString::fromStdString(text);
        if(header.compare(localheader) != 0)
        {
            localheader = header;
            if(header != "")
                log->append(QString::fromStdString(header));
        }

        log->append(buffer);
        QTextCursor c =  log->textCursor();
        c.movePosition(QTextCursor::End);
        log->setTextCursor(c);
    }
#else
    (void) header;
#endif
}

int Configuration::GetConfigMeans()
{
    return *configmeans;
}

bool Configuration::SetConfigMeans(int means)
{
    if(means < shiftreg || means > cmd)
        return false;

    *configmeans = means;
    return true;
}

bool Configuration::SeparateConfigMeans(int newmeans)
{
    if(newmeans < shiftreg || newmeans > cmd)
        return false;

    if(ownconfigmeans)
        *configmeans = newmeans;
    else
    {
        ownconfigmeans = true;
        configmeans = new int(newmeans);
    }

    return true;
}

bool Configuration::SendUpdateViaSR(int configflags, bool print)
{
    if(!nexys->is_open() || configflags == 0)
        return false;

    unsigned int daclength = atlaspix_dac->GenerateBitVector().size();
    unsigned int configlength = atlaspix_config->GenerateBitVector().size();
    unsigned int vdaclength = atlaspix_vdac->GenerateBitVector().size();
    //unsigned int tdaclength = atlaspix_tdac->GenerateBitVector().size();
    unsigned int rowlength  = atlaspix_row->GenerateBitVector().size();
    unsigned int columnlength = atlaspix_column->GenerateBitVector().size();
    unsigned int total = 0;
    if(configflags & dac)
        total += daclength;
    if(configflags & config)
        total += configlength;
    if(configflags & vdac)
        total += vdaclength;
    if(configflags & row)
        total += rowlength;
    if(configflags & column)
        total += columnlength;
    //total += tdaclength;

    if(!progressbarblocked)
    {
        SetProgressBarMaximum(int(total));
        SetProgressBarValue(0);
        ProcessEvents();
    }

    bool result = true;



//(unsigned char address, std::vector<bool> values, unsigned char Sin, unsigned char ld, bool sendload, const int clockdiv)


    if(configflags & dac)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_dac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(daclength));
            ProcessEvents();
        }
    }
    if(configflags & config)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_config->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(configlength));
            ProcessEvents();
        }
    }
    if(configflags & vdac)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_vdac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(vdaclength));
            ProcessEvents();
        }
    }
    //result &= nexys->WriteASIC(0x00, atlaspix_tdac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
    //if(!progressbarblocked)
    //{
    //    SetProgressBarValue(GetProgressBarValue() + int(tdaclength));
    //    ProcessEvents();
    //}
    if(configflags & row)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_row->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(rowlength));
            ProcessEvents();
        }
    }
    if(configflags & column)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_column->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(columnlength));
            ProcessEvents();
        }
    }

    if(print)
    {
        if(result && nexys->Flush())
            logit("Configurating chip successful.");
        else
            logit("Configuration of the chip failed...");
    }

    return result;
}

bool Configuration::SendUpdateNoSR(int configflags, bool print)
{
    if(!nexys->is_open() || configflags == 0)
        return false;

    unsigned int daclength = atlaspix_dac->GenerateBitVector().size();
    unsigned int configlength = atlaspix_config->GenerateBitVector().size();
    unsigned int vdaclength = atlaspix_vdac->GenerateBitVector().size();
    //unsigned int tdaclength = atlaspix_tdac->GenerateBitVector().size();
    unsigned int rowlength  = atlaspix_row->GenerateBitVector().size();
    unsigned int columnlength = atlaspix_column->GenerateBitVector().size();
    unsigned int total = 0;
    if(configflags & dac)
        total += daclength;
    if(configflags & config)
        total += configlength;
    if(configflags & vdac)
        total += vdaclength;
    if(configflags & column)
        total += columnlength;
    if(configflags & row)
        total += rowlength;
    //total += tdaclength;

    if(!progressbarblocked)
    {
        SetProgressBarMaximum(int(total));
        SetProgressBarValue(0);
        ProcessEvents();
    }
    bool result = true;

    if(configflags & dac)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_dac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                   NexysIO::SinA, NexysIO::Ld, false);
        result &= nexys->Write(16, 1 + 128, false, 20 * 8); //LdDAC & output enable
        result &= nexys->Write(16, 128, false, 8);          //LdDAC off & output enable
        result &= nexys->Write(16, 0, false, 8);            //disable output
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(daclength));
            ProcessEvents();
        }
    }
    if(configflags & config)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_config->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                   NexysIO::SinA, NexysIO::Ld, false);
        result &= nexys->Write(16, 2 + 128, false, 20 * 8);
        result &= nexys->Write(16, 128, false, 8);
        result &= nexys->Write(16, 0, false, 8);
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(configlength));
            ProcessEvents();
        }
    }
    if(configflags & vdac)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_vdac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                   NexysIO::SinA, NexysIO::Ld, false);
        result &= nexys->Write(16, 4 + 128, false, 20 * 8);
        result &= nexys->Write(16, 128, false, 8);
        result &= nexys->Write(16, 0, false, 8);
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(vdaclength));
            ProcessEvents();
        }
    }
    //result &= nexys->WriteASIC(0x00, atlaspix_tdac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
    //                            NexysIO::SinA, NexysIO::Ld, false);
    //result &= nexys->Write(16, 8 + 128, false, 20 * 8);
    //result &= nexys->Write(16, 128, false, 8);
    //result &= nexys->Write(16, 0, false, 8);
    //if(!progressbarblocked)
    //{
    //    SetProgressBarValue(GetProgressBarValue() + int(tdaclength));
    //    ProcessEvents();
    //}
    if(configflags & row)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_row->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                   NexysIO::SinA, NexysIO::Ld, false);
        result &= nexys->Write(16, 16 + 128, false, 20 * 8);
        result &= nexys->Write(16, 128, false, 8);
        result &= nexys->Write(16, 0, false, 8);
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(rowlength));
            ProcessEvents();
        }
    }
    if(configflags & column)
    {
        result &= nexys->WriteASIC(0x00, atlaspix_column->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                   NexysIO::SinA, NexysIO::Ld, false);
        result &= nexys->Write(16, 32 + 128, false, 20 * 8);
        result &= nexys->Write(16, 128, false, 8);
        result &= nexys->Write(16, 0, false, 8);
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + int(columnlength));
            ProcessEvents();
        }
    }

    if(print)
    {
        if(result && nexys->Flush())
            logit("Configurating chip successful.");
        else
            logit("Configuration of the chip failed...");
    }

    return result;
}

bool Configuration::SendUpdateSPI(int configflags, bool print)
{
    if(!nexys->is_open() || configflags == 0)
        return false;

    unsigned int daclength = atlaspix_dac->GenerateBitVector().size() - 3 * 16;
    unsigned int configlength = atlaspix_config->GenerateBitVector().size() - 3 * 16;
    unsigned int vdaclength = atlaspix_vdac->GenerateBitVector().size() - 3 * 16;
    //unsigned int tdaclength = atlaspix_tdac->GenerateBitVector().size() - 3 * 16;
    unsigned int rowlength  = atlaspix_row->GenerateBitVector().size() - 3 * 16;
    unsigned int columnlength = atlaspix_column->GenerateBitVector().size() - 3 * 16;
    unsigned int total = 0;
    if(configflags & dac)
        total += daclength;
    if(configflags & config)
        total += configlength;
    if(configflags & vdac)
        total += vdaclength;
    if(configflags & row)
        total += rowlength;
    if(configflags & column)
        total += columnlength;
    //total += tdaclength;

    if(!progressbarblocked)
    {
        SetProgressBarMaximum(int(total));
        SetProgressBarValue(0);
        ProcessEvents();
    }
    bool result = true;

#ifndef _useQT_
    void* progress = nullptr;
#endif

    void* disableprogress = nullptr;
    if(!progressbarblocked)
        disableprogress = progress;

    if(configflags & dac)
        result &= spiconfig->WriteASIC(nexys, atlaspix_dac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                      SPINexys::LdDAC, disableprogress, waiter);
    if(configflags & config)
        result &= spiconfig->WriteASIC(nexys, atlaspix_config->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                      SPINexys::LdConfig, disableprogress, waiter);
    if(configflags & vdac)
        result &= spiconfig->WriteASIC(nexys, atlaspix_vdac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                      SPINexys::LdVDAC, disableprogress, waiter);
    //result &= spiconfig->WriteASIC(nexys, atlaspix_tdac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
    //                                SPINexys::LdTDAC, disableprogress, waiter);
    if(configflags & row)
        result &= spiconfig->WriteASIC(nexys, atlaspix_row->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                        SPINexys::LdRow, disableprogress, waiter);
    if(configflags & column)
        result &= spiconfig->WriteASIC(nexys, atlaspix_column->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                                        SPINexys::LdColumn, disableprogress, waiter);

    if(print)
    {
        nexys->Flush();
        if(result) // && nexys->Flush())
            logit("Configurating chip successful.");
        else
            logit("Configuration of the chip failed...");
    }

    return result;

}

bool Configuration::SendUpdateCMD(int configflags, bool print)
{
    if(!nexys->is_open() || configflags == 0)
        return false;

    const int syncwordsperblock = 60;   //it is crucial to have this value > 50 or the
                                        //  written data in the chip will be corrupted!!!
    bool result = true;
    std::vector<byte> cmd;

    int total = 0;
    if(configflags & config)
        ++total;
    if(configflags & dac)
        ++total;
    if(configflags & vdac)
        ++total;
    if(configflags & row)
        ++total;
    if(configflags & column)
        ++total;

    if(!progressbarblocked)
    {
        SetProgressBarMaximum(int(total));
        SetProgressBarValue(0);
        ProcessEvents();
    }

    if(configflags & config)
    {
        cmd = cmdc->GenerateRegWriteByteCode(16,
                    atlaspix_config->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                    CMDConfig::ldconfig, syncwordsperblock);
        result &= SendCMDSplit(cmd);
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + 1);
            ProcessEvents();
        }
    }
    if(configflags & dac)
    {
        cmd = cmdc->GenerateRegWriteByteCode(16,
                    atlaspix_dac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                    CMDConfig::lddac, syncwordsperblock);
        result &= SendCMDSplit(cmd);
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + 1);
            ProcessEvents();
        }
    }
    if(configflags & vdac)
    {
        cmd = cmdc->GenerateRegWriteByteCode(16,
                    atlaspix_vdac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                    CMDConfig::ldvdac, syncwordsperblock);
        result &= SendCMDSplit(cmd);
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + 1);
            ProcessEvents();
        }
    }
    if(configflags & row)
    {
        cmd = cmdc->GenerateRegWriteByteCode(16,
                    atlaspix_row->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                    CMDConfig::ldrow, syncwordsperblock);
        result &= SendCMDSplit(cmd);
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + 1);
            ProcessEvents();
        }
    }
    if(configflags & column)
    {
        cmd = cmdc->GenerateRegWriteByteCode(16,
                    atlaspix_column->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                    CMDConfig::ldcolumn, syncwordsperblock);
        result &= SendCMDSplit(cmd);
        if(!progressbarblocked)
        {
            SetProgressBarValue(GetProgressBarValue() + 1);
            ProcessEvents();
        }
    }

    if(print)
    {
        //nexys->Flush(); //the flush is already done by the Write() method
        if(result)
            logit("Configurating chip successful.");
        else
            logit("Configuration of the chip failed...");
    }

    return result;
}

bool Configuration::SendUpdate(int configflags, bool print)
{
    switch(*configmeans)
    {
    case(shiftreg):
        return SendUpdateViaSR(configflags, print);
    case(noshiftreg):
        return SendUpdateNoSR(configflags, print);
    case(spi):
        return SendUpdateSPI(configflags, print);
    case(cmd):
        return SendUpdateCMD(configflags, print);
    default:
        logit("called with unknown configuration method", "", true);
        return false;
    }
}

std::vector<bool> Configuration::ReadBack(int configflags, bool print)
{
    if(*configmeans != spi)
    {
        logit("Readback currently only implemented for SPI readout");
        return std::vector<bool>();
    }

    int length = 0;
    int load = 0;
    switch(configflags)
    {
    case(config):
        length = atlaspix_config->GetTotalBits(true);
        load = SPINexys::LdConfig;
        break;
    case(dac):
        length = atlaspix_dac->GetTotalBits(true);
        load = SPINexys::LdDAC;
        break;
    case(vdac):
        length = atlaspix_vdac->GetTotalBits(true);
        load = SPINexys::LdVDAC;
        break;
    case(tdac):
        length = atlaspix_tdac->GetTotalBits(true);
        load = SPINexys::LdTDAC;
        break;
    case(row):
        length = atlaspix_row->GetTotalBits(true);
        load = SPINexys::LdRow;
        break;
    case(column):
        length = atlaspix_column->GetTotalBits(true);
        load = SPINexys::LdColumn;
        break;
    default:
        length = 0;
        load = 0;
        break;
    }

    return spiconfig->ReadBack(nexys, load, length, ((progressbarblocked)?nullptr:progress),
                               ((progressbarblocked)?nullptr:waiter), print);
}

std::vector<bool> Configuration::ReadBackRAMBitSPI(int row, int bit, bool print)
{
    if(bit < 0 || bit > 3 || row < 0 || row > AP3rows)
        return std::vector<bool>();

    //turn off write drivers for RAM (and save the state for restoring afterwards):
    bool wrramen = spiconfig->GetWrRAMEnState();
    if(wrramen)
        spiconfig->SetWrRAMEnState(false);

    spiconfig->WriteLineState(nexys);

    //select the row to read from:
    for(int i = 0; i < AP3rows; ++i)
    {
        std::stringstream s("");
        s << "enWrRAM_row_" << i;
        atlaspix_row->SetParameter(s.str(), ((i == row)?1:0));
    }
    //write Row Register without GUI updating:
    spiconfig->WriteASIC(nexys, atlaspix_row->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, true),
                         SPINexys::LdRow);

    //apply precharge to the bit lines of the RAM:
    spiconfig->SetPCHState(true);
    spiconfig->WriteLineState(nexys, true);
    spiconfig->SetPCHState(false);
    spiconfig->WriteLineState(nexys, true);

    //turn on only one WriteRAM signal:
    for(int i = 0; i < 4; ++i)
        spiconfig->SetWrRAMState(i, i == bit);
    spiconfig->WriteLineState(nexys, true);

    std::vector<bool> tdacbits = ReadBack(tdac, print);

    spiconfig->SetWrRAMState(bit, false);
    spiconfig->WriteLineState(nexys, true);

    if(wrramen)
        spiconfig->SetWrRAMEnState(true);

    return tdacbits;
}

std::vector<bool> Configuration::ReadBackRAMBit(int row, int bit, bool print)
{
    if(*configmeans != spi)
    {
        logit("RAM readback is currently only implemented for SPI");
        return std::vector<bool>();
    }
    else if(bit < 0 || bit > 3)
    {
        logit("Bit Number for RAM reading out of range");
        return std::vector<bool>();
    }
    else if(row < 0 || row > AP3rows)
    {
        logit("Row number out of range");
        return std::vector<bool>();
    }

    return ReadBackRAMBitSPI(row, bit, print);
}

std::vector<int> Configuration::ReadBackRAMRow(int row, bool print)
{
    if(row < 0 || row > AP3rows)
    {
        logit("Row index out of range");
        return std::vector<int>();
    }

    std::vector<int> tdacresult;
    tdacresult.resize(AP3columns, 0);

    for(int i = 0; i < 4; ++i)
    {
        std::vector<bool> tdacvalue = ReadBackRAMBit(row, i, print);

        //the content of the resulting vector is reversed: Column 0 is at the back
        for(unsigned int j = 0; j < tdacvalue.size(); ++j)
            tdacresult[AP3columns - 1 - j] |= ((tdacvalue[j])?(1 << i):0);
    }

    return tdacresult;
}

TDAC_Config Configuration::ReadBackRAMMatrix(bool* running, bool print, int start, int stop, bool block)
{
    if(running == nullptr || *running == false || start > stop || start < 0 || stop >= AP3rows)
        return TDAC_Config();

    TDAC_Config tdacvalues = *tdacs;

    if(block)
        BlockProgressBar();
    bool selfblocked = true;
#ifdef _useQT_
    if(selfblocked)
    {
        SetProgressBarMaximum(stop - start + 1);
        SetProgressBarValue(0);
    }
#endif

    for(int row = start; row <= stop && *running; ++row)
    {
        if(print)
        {
            std::stringstream s("");
            s << "Reading RAM of Row " << row;
            logit(s.str());
        }
        else
            std::cout << "Reading RAM of Row " << row << "\r" << std::flush;

        std::vector<int> tdacrow = ReadBackRAMRow(row, print);

        //copy the settings to the complete matrix representation:
        for(unsigned int i = 0; i < tdacrow.size(); ++i)
            tdacvalues.SetTDACValue(i, row, tdacrow[i]);

        if(selfblocked)
        {
            SetProgressBarValue(row - start);
            ProcessEvents();
        }
    }

    if(selfblocked)
    {
        SetProgressBarValue(stop - start + 1);
        ReleaseProgressBar();
    }

    return tdacvalues;
}

bool Configuration::SendCMDSplit(std::vector<byte> &cmd)
{
    if(((nexys->Read(50,1))[0] & 1) == 0)
    {
        logit("CMD Decoder Module not enabled. Aborting");
        return false;
    }

    //send short commands at once (i.e. longer than the FIFO):
    if(cmd.size() < 1024)
        return nexys->Write(51, cmd, true, 1);
    //split long commands
    else
    {
        bool result = true;
        std::vector<byte> cmd_part;
        //process 167 commands at once (1002 / 6 = 167):
        for(unsigned int loops = 0; loops < cmd.size(); loops += 1002)
        {
            //wait for complete processing of one part before sending the next:
            while(((nexys->Read(50, 1))[0] & 4) == 0)
                Timing::Sleep(20);

            std::cout << "preparing sending of part " << (loops/1002 + 1) << std::endl;

            cmd_part.clear();
            for(unsigned int i = 0; i < 1002 && loops + i < cmd.size(); ++i)
                cmd_part.push_back(cmd[i + loops]);

            result &= nexys->Write(51, cmd_part, true, 1);
        }

        return result;
    }
}

bool Configuration::ConfigurePins(bool flush)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    bool status = true;
    status &= nexys->Write(static_cast<unsigned char>(fm_resets->GetFPGAAddress()),
                           byte(fm_resets->GetConfiguration()), false, 1);
    status &= nexys->Write(static_cast<unsigned char>(fm_configmode->GetFPGAAddress()),
                           byte(fm_configmode->GetConfiguration()), false, 1);
    status &= nexys->Write(static_cast<unsigned char>(fm_workmode->GetFPGAAddress()),
                           byte(fm_workmode->GetConfiguration()), flush, 1);

    return status;
}

bool Configuration::ConfigureInjections(bool flush)
{
    if(!nexys->is_open())
        return false;

    bool status = true;

    //signal strength:
    status &= nexys->WritePCB(NexysIO::FPGA_VOLTAGEBOARD_CONFIG, injection->GenerateBitVector(), 8);

    //sync and output settings:
    status &= nexys->Write(Injection_Config::OutputSyncAddress,
                           byte(injection->GetSynced() * Injection_Config::SyncFlag
                           + (injection->GetOutputChannel() & ~Injection_Config::SyncFlag)
                                + injection->GetTSOverflowSync() * 8));

    //clock divider:
    int clockdiv = int(injection->GetClockDiv());
    if(clockdiv > 0)
        --clockdiv;

    //timing, number of pulse sets:
    nexys->PatGen(int(injection->GetPeriod()), int(injection->GetNumPulseSets()),
                                clockdiv, int(injection->GetInitDelay()));

    //pulses in a set:
    status &= nexys->PatGenWrite(7, static_cast<unsigned char>(injection->GetNumPulsesInaSet()));

    if(flush)
        nexys->Flush();

    return status;
}

bool Configuration::StartInjections(bool flush)
{
    nexys->PatGenSuspend(1);
    nexys->PatGenReset(1);
    nexys->PatGenReset(0);
    nexys->PatGenSuspend(0);
    if(flush)
        return nexys->Flush();
    else
        return true;
}

bool Configuration::RestartInjections(bool flush)
{
    nexys->PatGenReset(1);
    nexys->PatGenReset(0);
    nexys->PatGenSuspend(0);
    if(flush)
        return nexys->Flush();
    else
        return true;
}

bool Configuration::StopInjections(bool flush)
{
    nexys->PatGenSuspend(true);
    nexys->PatGenReset(true);
    if(flush)
        return nexys->Flush();
    else
        return true;
}

bool Configuration::WriteRAMRow(int row, bool flush, bool allseven)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;




    //select the row of interest (and turn off all others):
    for(int i = 0; i < AP3rows; ++i)
    {
        std::stringstream s("");
        s << "WrRAM_" << i;
        atlaspix_dac->SetParameter(s.str(), (i == row)?1:0);
    }

    bool result = true;

    result &= nexys->WriteASIC(0x00, atlaspix_dac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
    result &= nexys->Flush();
/*
    switch(*configmeans)
    {
        case(shiftreg):
            //logit("WrTDAC using SR");
            result &= nexys->WriteASIC(0x00, atlaspix_dac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
            result &= nexys->Flush();
        break;

        default:
            logit("TDAC writing not implemented for this configuration version");
            return false;
    }

    */

    if(!result)
    {
        logit("Writing RAM not successful");
        return false;
    }



    for(unsigned int col = 0; col < AP3columns; ++col)
    {
        std::stringstream s("");
        s << "TuneDAC_" << col;
/*
        //Ivan v2 chip

        if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 15) atlaspix_dac->SetParameter(s.str(), 8);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 14) atlaspix_dac->SetParameter(s.str(), 9);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 13) atlaspix_dac->SetParameter(s.str(), 10);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 12) atlaspix_dac->SetParameter(s.str(), 11);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 11) atlaspix_dac->SetParameter(s.str(), 12);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 10) atlaspix_dac->SetParameter(s.str(), 13);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) ==  9) atlaspix_dac->SetParameter(s.str(), 14);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) ==  8) atlaspix_dac->SetParameter(s.str(), 15);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 7) atlaspix_dac->SetParameter(s.str(), 0);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 6) atlaspix_dac->SetParameter(s.str(), 1);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 5) atlaspix_dac->SetParameter(s.str(), 2);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 4) atlaspix_dac->SetParameter(s.str(), 3);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 3) atlaspix_dac->SetParameter(s.str(), 4);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) == 2) atlaspix_dac->SetParameter(s.str(), 5);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) ==  1) atlaspix_dac->SetParameter(s.str(), 6);
        else if(tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) ==  0) atlaspix_dac->SetParameter(s.str(), 7);
*/
        //Ivan v3 chip
        if(allseven){
            std::cout << "writing 7 in" << s.str() << std::endl;
            atlaspix_tdac->SetParameter(s.str(), 7);
        }


        else atlaspix_dac->SetParameter(s.str(), tdacs->GetTDACValue(col,static_cast<unsigned int>(row)));


    }

    result &= nexys->WriteASIC(0x00, atlaspix_dac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
    result &= nexys->Flush();

/*

    for(int i = 1; i < 16; i <<= 1)
    {
        for(unsigned int col = 0; col < AP3columns; ++col)
        {
            std::stringstream s("");
            s << "TuneDAC_" << col;
            atlaspix_dac->SetParameter(s.str(), (((tdacs->GetTDACValue(col,static_cast<unsigned int>(row)) & i) != 0)?1:0));


        }

        result &= nexys->WriteASIC(0x00, atlaspix_dac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
        result &= nexys->Flush();
        */

/*
        switch(*configmeans)
        {
            case(shiftreg):


            break;

            default:
                logit("TDAC writing not implemented for this configuration version");
                return false;
        }



    }
    */

    /*

    if(flush)
    {
        bool result1 = result;
        result &= nexys->Flush();

        if(result1 && !result)
            std::cout << "fail at extra flush" << std::endl;
    }

    */


    //prevent accidental RAM writing by turning off again the wrRAM flags:
    for(int i = 0; i < AP3rows; ++i)
    {
        std::stringstream s("");
        s << "WrRAM_" << i;
        atlaspix_dac->SetParameter(s.str(), 0);
    }

    result &= nexys->WriteASIC(0x00, atlaspix_dac->GenerateBitVector(ASIC_Config2::GlobalInvertedLSBFirst, false));
    result &= nexys->Flush();


    if(!result)
        logit("Writing RAM not successful");


    return result;
}

bool Configuration::WriteRAMMatrix()
{
    if(nexys == nullptr || !nexys->is_open())
        return false;



    bool result = true;
    if(!progressbarblocked)
    {
        SetProgressBarMaximum(AP3rows);
        SetProgressBarValue(0);
    }
    //enable RAM writing for CMD decoder (if not already on):


    for(int i = 0; i < AP3rows && result; ++i)
    {
        result &= WriteRAMRow(i, false);
        if(!progressbarblocked)
        {
            SetProgressBarValue(i);
            ProcessEvents();
        }


    }




    //result &= nexys->Flush();


    if(!progressbarblocked)
        SetProgressBarValue(AP3rows);

    return result;
}

bool Configuration::ConfigureVoltageBoards(bool flush)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    const int clockdiv = 8;

    bool status = true;
    status &= nexys->WritePCB(NexysIO::FPGA_VOLTAGEBOARD_CONFIG, voltageboards[0]->GenerateBitVector(), clockdiv);
    status &= nexys->WritePCB(NexysIO::FPGA_VOLTAGEBOARD_CONFIG, voltageboards[1]->GenerateBitVector(), clockdiv);

    if(flush)
        status &= nexys->Flush();

    return status;
}

bool Configuration::ConfigureFastReadout(bool flush)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    bool status = true;
    //also send the other parameters to make sure they are as set in the GUI:
    status &= nexys->Write(FastReadout::TrigDelayAddress, byte(fastro->GetTriggerDelay()),false);
    status &= nexys->Write(FastReadout::TrigLengthAddress, byte(fastro->GetTriggerLength()), false);

    //clock divider from chip configuration:
    status &= nexys->Write(FastReadout::TSDiv, byte(atlaspix_config->GetParameter("ckdivend")), false);
    status &= nexys->Write(FastReadout::TS2Div, byte(atlaspix_config->GetParameter("ckdivend2")), false);
    status &= nexys->Write(FastReadout::TSPhase, byte(fastro->GetTSPhase()), true);
    if(flush)
        status &= nexys->Flush();

    return status;
}

bool Configuration::StartFastReadout(bool flush, bool configure)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    bool status = true;
    if(configure)
        status &= ConfigureFastReadout(false);
    status &= nexys->Write(FastReadout::FPGAAddress, byte(fastro->SetEnabled(true)));
    if(flush)
        status &= nexys->Flush();

    return status;
}

bool Configuration::StopFastReadout(bool flush)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;
    bool status = true;
    status &= nexys->Write(FastReadout::FPGAAddress, byte(fastro->SetEnabled(false)));;
    if(flush)
        status &= nexys->Flush();

    return status;
}

bool Configuration::ResetFastReadout(int flags, bool flush)
{
    fastro_lastread = "";

    if((fastro->GetConfiguration(true) & (FastReadout::reset + FastReadout::fifoclear)) == 0)
    {
        logit("Nothing to do here. Quitting...");
        return true;
    }

    std::vector<byte> reset;

    if(flags == -1)
        reset.push_back(byte(fastro->GetConfiguration(true)));
    else
        reset.push_back(byte(fastro->GetConfiguration(false) | flags));
    reset.push_back(byte(fastro->GetConfiguration(false)));

    bool status = true;
    status &= nexys->Write(FastReadout::FPGAAddress, reset, flush, 4);

    return status;




}

std::vector<Dataset> Configuration::ReadoutAll(bool *running, bool print, unsigned int maxnumhits,
                                               int maxemptyreads, int readsize, int maxtime)
{
    bool localrunner = true;
    if(running == nullptr)
        running = &localrunner;

    if(nexys == nullptr || !nexys->is_open() || !(*running) || readsize <= 0)
        return std::vector<Dataset>();

    if(print)
    {
        logit("Reading Data ...");
        ProcessEvents();
    }

    std::vector<Dataset> hits;
    std::map<Dataset, unsigned int> hitcnter;
    int counter = 0;

    bool datamux   = fastro->GetDataMuxEnable();
    bool triggered = fastro->GetReadoutMode();
    std::string answer = "";

    int endcondition = (datamux)?3:12;

    Timing::TimePoint start = Timing::GetTimeNow();

    while(*running && counter < maxemptyreads)
    {
        answer += nexys->Read(NexysIO::FPGA_READOUT_FIFO, 12240);//Ivan  8 * 12 * 100
        //quick check for data sanity (blocks of 8 byte for a dataset):
        if((answer.length() % 8) != 0)
            std::cout << "reading alignment problem..." << std::endl;
        answer = FastReadout::RemoveEmptyData(answer);
        std::vector<Dataset> newhits;
        if(answer != "")
        {
            if(triggered)
            {
                newhits = fastro->DecodeManyTrigger(answer);
                answer = "";
            }
            else
            {
                answer = FastReadout::RemoveEmptyData(answer);
                newhits = FastReadout::DecodeMany(datamux, answer);
                unsigned int linestart = (answer.length() / 8 - 1) * 8; //start of the last line in the data
                //last dataset is complete:
                if(answer.c_str()[linestart] == endcondition)
                    answer = "";
                //remove all but the last (incomplete) dataset:
                else
                {
                    for(unsigned int i = 1; i < 10; ++i)
                    {
                        if(int(linestart) - int(i * 8) >= 0 && answer.c_str()[linestart - i * 8] == endcondition)
                        {
                            answer = answer.substr(linestart - (i-1) * 8);
                            break;
                        }
                    }
                    if(answer.length() == linestart + 7)
                        answer = "";
                }
            }

            //add the newly read hits to the list of read hits:
            hits.insert(hits.end(), newhits.begin(), newhits.end());
            //insert them into the hit counter:
            for(auto& it : newhits)
            {
                auto elem = hitcnter.find(it);
                if(elem != hitcnter.end())
                    ++(elem->second);
                else
                    hitcnter.insert(std::make_pair(it, 1));
            }
            //abort on too many hits:
            for(auto& it : hitcnter)
            {
                if(it.second > maxnumhits)
                    break;
            }
            //if(hits.size() > maxnumhits && maxnumhits > 0)
            //    break;
            //abort on reading longer than 10s:
            if(Timing::TimesToIntervalMS(start, Timing::GetTimeNow()) > maxtime)
                break;
        }

        //no data counter:
        if(newhits.size() == 0)
            ++counter;
        else
        {
            counter = 0;
            if(print)
                ProcessEvents();
        }
    }

    if(print)
    {
        std::stringstream s("");
        s << "  -> in total: " << hits.size() << " hits" << std::endl;
        logit(s.str());
    }

    return hits;
}

std::map<Dataset, int> Configuration::SortHits(const std::vector<Dataset> &data, bool print, bool IDreject)
{
    std::map<Dataset, int> sortedhits;
    std::map<Dataset, int> lastids;     //for storing the last IDs for the pixel to reject double hits
    for(const auto& it : data)
    {
        auto entry = sortedhits.find(it);
        auto identry = lastids.find(it);

        if(entry != sortedhits.end())
        {
            if(!IDreject || it.triggerid != identry->second)
                ++(entry->second);
            identry->second = it.triggerid;
        }
        else
        {
            sortedhits.insert(std::make_pair(it, 1));
            lastids.insert(std::make_pair(it, it.triggerid));
        }
    }

    if(print)
    {
        std::stringstream s("");
        s << Dataset::GetStringHeader(true) << ";\t-> Count" << std::endl;
        int integral = 0;
        for(const auto& it : sortedhits)
        {
            s << it.first.ToString(true) << "\t-> " << it.second << std::endl;
            integral += it.second;
        }
        s << "  -> in total: " << integral << " hits" << std::endl;

        logit(s.str());
    }

    return sortedhits;
}

bool Configuration::WriteHitsToFile(const std::vector<Dataset> &hits, std::string filename)
{
    std::fstream f;
    f.open(filename.c_str(), std::ios::out | std::ios::app);
    if(!f.is_open())
        return false;

    std::stringstream s("");
    for(const auto& it : hits)
        s << it.ToString(true) << std::endl;

    f << s.str();
    f.flush();

    f.close();
    return true;
}

Configuration::blthr Configuration::RaiseThreshold()
{
    blthr lowthreshold;
    lowthreshold.bl  = atlaspix_vdac->GetParameter("BL");
    lowthreshold.thr = atlaspix_vdac->GetParameter("Th");
    if(lowthreshold.thr - lowthreshold.bl < 25)
    {
        if(lowthreshold.bl + 50 < 255)
            atlaspix_vdac->SetParameter("Th", lowthreshold.bl + 50);
        else
            atlaspix_vdac->SetParameter("BL", lowthreshold.thr - 50);

        SendUpdate(Configuration::vdac, false);
        nexys->Flush();
        Timing::Sleep(250);
    }

    return lowthreshold;
}

void Configuration::RecoverThreshold(Configuration::blthr thr)
{
    //nothing to be done for unchanged thresholds:
    if(atlaspix_vdac->GetParameter("BL") == int(thr.bl)
            && atlaspix_vdac->GetParameter("Th") == int(thr.thr))
        return;

    atlaspix_vdac->SetParameter("BL", thr.bl);
    atlaspix_vdac->SetParameter("Th", thr.thr);

    SendUpdate(Configuration::vdac, false);
    nexys->Flush();
    Timing::Sleep(250);
}
