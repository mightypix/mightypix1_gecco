/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spinexys.h"

SPINexys::SPINexys(unsigned int buffersize)  : buffercontent(0), writeaddress(23), readaddress(24),
                clockdivaddress(22), clockdiv(10), configurationaddress(-1), lastreadback("")
{
    identifier = "SPI Configuration";
    this->buffersize = buffersize;

    fm_config.AddFlag("WrFIFORst"  ,   1, false);
    fm_config.AddFlag("WrFIFOEmpty",   2, false);
    fm_config.AddFlag("WrFIFOFull" ,   4, false);
    fm_config.AddFlag("RdFIFORst"  ,   8, false);
    fm_config.AddFlag("RdFIFOEmpty",  16, false);
    fm_config.AddFlag("RdFIFOFull" ,  32, false);
    fm_config.AddFlag("ReadbackEn" ,  64, false);
    fm_config.AddFlag("SPIModuleEn", 128, false);

    fm_lines.AddFlag("Sin"      , Sin       , false);
    fm_lines.AddFlag("Ck1"      , Ck1       , false);
    fm_lines.AddFlag("Ck2"      , Ck2       , false);
    fm_lines.AddFlag("Rb"       , Rb        , false);
    fm_lines.AddFlag("LdDAC"    , LdDAC     , false);
    fm_lines.AddFlag("LdConfig" , LdConfig  , false);
    fm_lines.AddFlag("LdVDAC"   , LdVDAC    , false);
    fm_lines.AddFlag("LdTDAC"   , LdTDAC    , false);
    fm_lines.AddFlag("LdRow"    , LdRow     , false);
    fm_lines.AddFlag("LdColumn" , LdColumn  , false);
    fm_lines.AddFlag("WriteRAM0", WriteRAM0 , false);
    fm_lines.AddFlag("WriteRAM1", WriteRAM1 , false);
    fm_lines.AddFlag("WriteRAM2", WriteRAM2 , false);
    fm_lines.AddFlag("WriteRAM3", WriteRAM3 , false);
    fm_lines.AddFlag("WrRAMEn"  , WriteRAMEn, false);
    fm_lines.AddFlag("ResetTS"  , ResetTS   , false);
    fm_lines.AddFlag("Injection", Injection , false);
    fm_lines.AddFlag("SPCH"     , SPCH      , false);

    fm_manual.SetFPGAAddress(29);
    fm_manual.AddFlag("csb", 1, true);
    fm_manual.AddFlag("clock", 2, false);
    fm_manual.AddFlag("data", 4, false);
    fm_manual.AddFlag("manual_mode", 128, false);
}

std::string SPINexys::GetIdentifier() const
{
    return identifier;
}

void SPINexys::SetIdentifier(std::string name)
{
    identifier = name;
}

unsigned int SPINexys::GetBufferSize()
{
    return buffersize;
}

void SPINexys::SetBufferSize(unsigned int buffersize)
{
    this->buffersize = buffersize;
}

bool SPINexys::GetReadBackEnable()
{
    return fm_config.GetFlag("ReadbackEn");
}

bool SPINexys::SetReadBackEnable(NexysIO* nexys, bool enable, bool flush)
{
    fm_config.SetFlag("ReadbackEn", enable);

    if(nexys == nullptr || !nexys->is_open())
        return false;
    else
        return nexys->Write(configurationaddress, byte(fm_config.GetConfiguration()), flush);
}

unsigned char SPINexys::GetWriteAddress()
{
    return writeaddress;
}

void SPINexys::SetWriteAddress(unsigned char address)
{
    writeaddress = address;
}

unsigned char SPINexys::GetReadAddress()
{
    return readaddress;
}

void SPINexys::SetReadAddress(unsigned char address)
{
    readaddress = address;
}

unsigned char SPINexys::GetClockDivAddress()
{
    return clockdivaddress;
}

void SPINexys::SetClockDivAddress(unsigned char address)
{
    clockdivaddress = address;
}

unsigned char SPINexys::GetConfigAddress()
{
    return configurationaddress;
}

void SPINexys::SetConfigAddress(unsigned char address)
{
    configurationaddress = address;
}

unsigned char SPINexys::GetClockDivider()
{
    return clockdiv;
}

bool SPINexys::SetClockDivider(NexysIO *nexys, unsigned char clockdiv)
{
    this->clockdiv = clockdiv;

    if(nexys == nullptr || !nexys->is_open())
        return false;
    else {
        return nexys->Write(clockdivaddress, clockdiv);
    }
}

bool SPINexys::GetSPIEnable()
{
    return !fm_config.GetFlag("SPIModuleEn");
}

bool SPINexys::SetSPIEnable(NexysIO *nexys, bool enable)
{
    fm_config.SetFlag("SPIModuleEn", (enable)?0:1);

    if(nexys == nullptr || !nexys->is_open())
        return false;
    else
        return nexys->Write(configurationaddress, byte(fm_config.GetConfiguration()));
}

bool SPINexys::GetSPIMode()
{
    return fm_manual.GetFlag("manual_mode") != 0;
}

bool SPINexys::SetSPIMode(NexysIO *nexys, bool manual)
{
    if(!fm_manual.SetFlag("manual_mode", manual))
        return false;

    return nexys->Write(fm_manual.GetFPGAAddress(), fm_manual.GetConfiguration());

}

bool SPINexys::Update(NexysIO *nexys)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    bool status = true;

    status &= SetSPIEnable(nexys, GetSPIEnable());
    status &= SetReadBackEnable(nexys, GetReadBackEnable());
    status &= SetClockDivider(nexys, GetClockDivider());
    status &= SetSPIMode(nexys, GetSPIMode());

    return status;
}

bool SPINexys::GetWrRAMEnState()
{
    return fm_lines.GetFlag("WrRAMEn");
}

void SPINexys::SetWrRAMEnState(bool activate)
{
    fm_lines.SetFlag("WrRAMEn", activate);
}

bool SPINexys::GetPCHState()
{
    return fm_lines.GetFlag("SPCH");
}

void SPINexys::SetPCHState(bool activate)
{
    fm_lines.SetFlag("SPCH", activate);
}

bool SPINexys::GetWrRAMState(int bit)
{
    if(bit < 0 || bit > 3)
        return false;
    std::stringstream s("");
    s << "WriteRAM" << bit;
    return fm_lines.GetFlag(s.str());
}

void SPINexys::SetWrRAMStateFlag(int flag, bool value)
{
    int bit = 0;
    switch(flag)
    {
    case(1): bit = 0; break;
    case(2): bit = 1; break;
    case(4): bit = 2; break;
    case(8): bit = 3; break;
    default: return;
    }

    SetWrRAMState(bit, value);

    return;
}

int SPINexys::GetLineState() const
{
    return fm_lines.GetConfiguration();
}

void SPINexys::SetWrRAMState(int bit, bool value)
{
    if(bit < 0 || bit > 3)
        return;

    std::stringstream s("");
    s << "WriteRAM" << bit;
    fm_lines.SetFlag(s.str(), value);
}

bool SPINexys::WriteLineState(NexysIO *nexys, bool flush)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    return Write(nexys, fm_lines.GetConfiguration(), flush);
}

bool SPINexys::ResetModule(NexysIO *nexys)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    bool result = true;

    result &= SetSPIEnable(nexys, false);
    result &= SetSPIEnable(nexys, true);

    return result;
}

bool SPINexys::ResetWriteFIFO(NexysIO *nexys)
{
    if(nexys == nullptr && !nexys->is_open())
        return false;

    bool result = true;
    fm_config.SetFlag("WrFIFORst", 1);
    result &= nexys->Write(configurationaddress, byte(fm_config.GetConfiguration()));
    fm_config.SetFlag("WrFIFORst", 0);
    result &= nexys->Write(configurationaddress, byte(fm_config.GetConfiguration()));

    buffercontent = 0;

    return result;
}

bool SPINexys::ResetReadFIFO(NexysIO *nexys)
{
    if(nexys == nullptr && !nexys->is_open())
        return false;

    bool result = true;
    fm_config.SetFlag("RdFIFORst", 1);
    result &= nexys->Write(configurationaddress, byte(fm_config.GetConfiguration()), false, 4);
    fm_config.SetFlag("RdFIFORst", 0);
    result &= nexys->Write(configurationaddress, byte(fm_config.GetConfiguration()));

    return result;
}

std::string SPINexys::Read(NexysIO *nexys, unsigned int numchars)
{
    if(nexys == nullptr || !nexys->is_open())
        return "";

    return nexys->Read(readaddress, int(numchars));
}

std::vector<byte> SPINexys::WriteManual(int setting)
{
    std::vector<byte> cmd;

    int value = fm_manual.GetConfiguration() & 254; //clear CSB bit at 1

    cmd.push_back(value | 1); //CSB = 1
    cmd.push_back(value);     //CSB = 0
    cmd.push_back(value | 2); // clock = 1
    cmd.push_back(value);

    //empty clocks for readback:
    if(fm_config.GetFlag("readback") != 0)
    {
        for(int i = 0; i < 40; ++i)
        {
            cmd.push_back(value | 2);
            cmd.push_back(value);
        }
    }

    //send the actual configuration:
    for(int i = 0; i < 24; ++i)
    {
        //set data bit:
        value = (value & (~4)) | (((setting & (1 << (23 - i))) != 0)?4:0);
        cmd.push_back(value);
        cmd.push_back(value | 2);
        cmd.push_back(value);
    }

    cmd.push_back(value | 1); //turn on CSB again

    return cmd;
}

bool SPINexys::Write(NexysIO *nexys, int setting, bool flush)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    if(fm_manual.GetFlag("manual_mode") == 0)
    {
        if(buffersize >= buffercontent + wordsize)
            return WriteNoCheck(nexys, setting, flush);
        else
            return WriteNoNexysCheck(nexys, setting, flush);
    }
    else
    {
        std::vector<byte> cmd = WriteManual(setting);

        return nexys->Write(fm_manual.GetFPGAAddress(), cmd, flush, clockdiv);
    }
}

bool SPINexys::Write(NexysIO *nexys, std::vector<int> data, void* pb, WaitFunction waiter)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    if(fm_manual.GetFlag("manual_mode") != 0)
    {
        std::vector<byte> cmd;
        for(auto& it : data)
        {
            auto oneword = WriteManual(it);
            cmd.insert(cmd.end(), oneword.begin(), oneword.end());
        }

        if(cmd.size() * clockdiv > 64000)
        {
            bool result = true;
            unsigned int index = 0;
            const unsigned int numbytes = 64000 / clockdiv;
            while(index < cmd.size())
            {
                unsigned int end = index + numbytes;
                if(end > cmd.size())
                    end = cmd.size();
                result &= nexys->Write(fm_manual.GetFPGAAddress(), cmd, index, end, true, clockdiv);
                index += numbytes;
            }

            return result;
        }
        else
            return nexys->Write(fm_manual.GetFPGAAddress(), cmd, false, clockdiv);
    }

    // lower limit of the space left in the FIFO:
    int counter = (buffersize - buffercontent) / wordsize;

    bool success = true;

    unsigned int i = 0;
    while(i < data.size())
    {
        if(counter > 0)
        {
            std::vector<int> datapart;

            for(unsigned int index = 0; int(index) < counter && index < data.size() - i; ++index)
                datapart.push_back(data[i + index]);

            int numsteps = 0;
            if(int(data.size()) - int(i) < counter)
            {
                numsteps = data.size() - i;
                counter -= numsteps;
                i = data.size();
            }
            else
            {
                numsteps = counter;
                i += counter;
                counter = 0;
            }

            success &= WriteNoCheck(nexys, datapart);

#ifdef _useQT_SPI_
            if(pb != nullptr)
                (static_cast<QProgressBar*>(pb))->setValue(
                                        (static_cast<QProgressBar*>(pb))->value() + numsteps);
                    //update every 5 config words (i.e. 1 bit transmitted)
#endif
            if(waiter != nullptr)
                waiter();

        }
        else
        {
            Timing::Sleep(5);
                //increase the chance of finding the FIFO empty to speed up writing
            int result = nexys->Read(configurationaddress, 1).c_str()[0];
            if((result & fm_config.GetFirstBitPosition("WrFIFOEmpty")) != 0)
            {
                counter = buffersize / wordsize;
                buffercontent = 0;
            }
            else if((result & fm_config.GetFirstBitPosition("WrFIFOFull")) == 0)
            {
                counter = 1;
                buffercontent -= wordsize;
            }

            static int loopcount = 0;
            if(loopcount++ > 10)
            {
                loopcount = 0;
                if(waiter != nullptr)
                    waiter();
            }
        }
    }

    return success;
}

bool SPINexys::WriteASIC(NexysIO *nexys, std::vector<bool> data, int load, void* pb, WaitFunction waiter)
{
    if(nexys == nullptr || !nexys->is_open())
        return false;

    std::vector<int> config;


    int value = fm_lines.GetConfiguration();

    value &= ~(Ck1 + Ck2 + LdDAC + LdConfig + LdVDAC + LdTDAC + LdRow + LdColumn + WriteRAM0
               + WriteRAM1 + WriteRAM2 + WriteRAM3);

    for(const auto it : data)
    {
        if(it)
            value |= Sin;
        else
            value &= ~Sin;
        config.push_back(value);
        config.push_back(value | Ck1);
        config.push_back(value);
        config.push_back(value | Ck2);
        config.push_back(value);
    }

    if(load != 0)
    {
        for(int i = 0; i < 4; ++i)
            config.push_back(value | load);
        config.push_back(value);
    }

    return Write(nexys,config, pb, waiter);
}

std::vector<bool> SPINexys::ReadBack(NexysIO *nexys, int load, int length, void *pb, WaitFunction waiter,
                                     bool print)
{
    if(length == 0)
        return std::vector<bool>();

    if(nexys != nullptr && nexys->is_open())
    {
#ifdef _useQT_SPI_
        if(pb != nullptr)
        {
            (static_cast<QProgressBar*>(pb))->setMaximum(1 + (length + 9) / 10);
            (static_cast<QProgressBar*>(pb))->setValue(0);
        }
#endif

        std::vector<int> cmdrb, cmd;

        int config = fm_lines.GetConfiguration() & (Injection + SPCH + WriteRAMEn);

        //prepare loading the data from the registers in the shift register latches:
        //cmdrb.push_back(config | Rb      );
        //cmdrb.push_back(config | Rb      );
        //cmdrb.push_back(config | Rb      );
        cmdrb.push_back(config | Rb | Ck1);
        cmdrb.push_back(config | Rb | Ck1);
        cmdrb.push_back(config | Rb      );
        cmdrb.push_back(config | Rb      );
        cmdrb.push_back(config | Rb | Ck2);
        cmdrb.push_back(config | Rb | Ck2);
        cmdrb.push_back(config | Rb      );
        cmdrb.push_back(config);
        cmdrb.push_back(config);
        cmdrb.push_back(config);

        //prepare shifting out 10 bits from all shift registers:
        for(int i = 0; i < 10; ++i)
        {
            cmd.push_back(config | Ck1);
            cmd.push_back(config      );
            cmd.push_back(config | Ck2);
            cmd.push_back(config      );
        }

        bool rbsetting = GetReadBackEnable();

        //FPGA data readback off:
        SetReadBackEnable(nexys, false, false);

        ResetReadFIFO(nexys);

        //load back data in shifting register:
        Write(nexys, config | Rb, true); // send setting of RB as extra cycle to check whether the
                                         //    circuit is just too slow for direct writing
        Write(nexys, cmdrb, pb, waiter);

        //read the loaded data in bunches of 10 bits at a time:
        for(int i = 0; i < length; i += 10)
        {
            Write(nexys, cmd, pb, waiter); //shift 10 bits into monitor_config register on chip
            SetReadBackEnable(nexys, true); //FPGA readback on
            WriteNoCheck(nexys, config, true); //read back the content with a "dummy write"
                //now the dataset (of 64 bits) is in the read FIFO on the FPGA
            SetReadBackEnable(nexys, false);
                //FPGA readback off again (no useless data from shifting)
        }

        SetReadBackEnable(nexys, rbsetting); //restore Rb setting

        //retrieve the data from the FPGA FIFO:
        unsigned int readlength = 8 * ((length + 9) / 10);
        lastreadback = Read(nexys, readlength + 8); //read one dataset more than there should be
                                                    // -> then, the last one should be empty
        //continue reading until the last dataset is empty:
        int count = 100; //"timeout" counter
        while(count-- > 0 && lastreadback[lastreadback.length() - 1] != char(255))
            lastreadback = lastreadback + Read(nexys, readlength);

        //if there is "way too much" data, repeat the read back:
        if(count < 97 || lastreadback.length() > 2 * readlength)
        {
            ResetReadFIFO(nexys);
            std::cout << "reread due to strange data" << std::endl;
            return ReadBack(nexys, load, length, pb, waiter, print);
        }
        //completereadback = lastreadback;
        //remove the empty datasets at the end of the data (until minimum length is reached):
        while(lastreadback.length() > readlength
              && lastreadback.substr(lastreadback.length() - 8)
                    .compare("\xff\xff\xff\xff\xff\xff\xff\xff") == 0)
            lastreadback = lastreadback.substr(0, lastreadback.length() - 8);
        //strip the old data from the readback:
        if(lastreadback.length() > readlength)
            lastreadback = lastreadback.substr(lastreadback.length() - readlength);

        if(print)
            std::cout << "Read " << lastreadback.length() << " bytes -> "
                      << lastreadback.length() / 8 * 10 << " data bits per register" << std::endl;
    }

    //determine which part of the data is of interest (see load signal):
    int offset = 0;
    switch(load)
    {
        case(LdDAC):    offset =  4; break;
        case(LdConfig): offset = 14; break;
        case(LdVDAC):   offset = 24; break;
        case(LdTDAC):   offset = 34; break;
        case(LdRow):    offset = 44; break;
        case(LdColumn): offset = 54; break;
        default: return std::vector<bool>();
    }

    //extract the bits from the read data for the selected register (`load` pararmeter):
    std::vector<bool> bitvector;

    for(unsigned int i = 0; i < lastreadback.length(); i += 8)
    {
        unsigned long long tenbits = 0;
        for(int j = 0; j < 8; ++j)
            tenbits = tenbits * 256 + (uint(lastreadback[i+j]) & 255);

        for(int j = offset; j < offset + 10; ++j)
            bitvector.push_back((tenbits & (ulonglong(1) << (63 - j))) != 0);
    }
    if(int(bitvector.size()) > length)
        bitvector.resize(length); //strip additional "data" in case length is not a multiple of 10

    return bitvector;
}

tinyxml2::XMLError SPINexys::LoadFromXMLElement(tinyxml2::XMLElement *root)
{
    if(root == nullptr)
        return tinyxml2::XML_ERROR_FILE_NOT_FOUND;

    const char* nam;
    nam = root->Attribute("name");
    if(nam != nullptr)
        identifier = std::string(nam);

    int value;
    value = root->IntAttribute("writeaddr");
    if(value > 0)
        writeaddress = byte(value);
    value = root->IntAttribute("readaddr");
    if(value > 0)
        readaddress = byte(value);
    value = root->IntAttribute("clockdivaddr");
    if(value > 0)
        clockdivaddress = byte(value);
    value = root->IntAttribute("clockdiv");
    if(value > 0)
        clockdiv = byte(value);
    value = root->IntAttribute("buffersize");
    if(value > 0)
        buffersize = static_cast<unsigned int>(value);
    value = root->IntAttribute("configaddr");
    if(value > 0)
        configurationaddress = byte(value);
    value = root->IntAttribute("config");
    if(value >= 0)
        fm_config.SetConfiguration(value);
    value = root->IntAttribute("lines");
    if(value >= 0)
        fm_lines.SetConfiguration(value);
    value = root->IntAttribute("manual");
    if(value >= 0)
        fm_manual.SetConfiguration(value);

    return tinyxml2::XML_NO_ERROR;
}

tinyxml2::XMLError SPINexys::LoadFromXMLFile(std::string filename, bool compressed)
{
    if(compressed)
    {
        std::fstream f;
        f.open(filename, std::ios::in);
        if(!f.is_open())
            return tinyxml2::XML_ERROR_FILE_NOT_FOUND;

        zip_file archive;
        archive.load(filename);
        filename = filename.substr(0, filename.rfind('.')) + ".xml";

        archive.extract(filename);
    }

    tinyxml2::XMLDocument doc;
    tinyxml2::XMLError error = doc.LoadFile(filename.c_str());
    if(error != tinyxml2::XML_NO_ERROR)
        return error;

    tinyxml2::XMLNode* config = getNode(&doc, "SPIConfig");

    error = LoadFromXMLElement(config->ToElement());

    if(compressed)
    {
        if(remove(filename.c_str()) != 0)
            std::cout << "error deleting decompression helper file \"" << filename << "\""
                      << std::endl;
    }

    return error;
}

tinyxml2::XMLElement *SPINexys::SaveToXMLElement(tinyxml2::XMLDocument &doc, std::string devicename)
{
    tinyxml2::XMLElement* rootnode = doc.NewElement("SPIConfig");

    if(devicename == "")
        rootnode->SetAttribute("name", identifier.c_str());
    else
        rootnode->SetAttribute("name", devicename.c_str());

    rootnode->SetAttribute("clockdiv", clockdiv);
    rootnode->SetAttribute("buffersize", buffersize);
    rootnode->SetAttribute("writeaddr", writeaddress);
    rootnode->SetAttribute("readaddr", readaddress);
    rootnode->SetAttribute("configaddr", configurationaddress);
    rootnode->SetAttribute("clockdivaddr", clockdivaddress);
    rootnode->SetAttribute("config", fm_config.GetConfiguration());
    rootnode->SetAttribute("lines", fm_lines.GetConfiguration());
    rootnode->SetAttribute("manual", fm_manual.GetConfiguration());

    return rootnode;
}

tinyxml2::XMLError SPINexys::SaveToXMLFile(std::string filename, std::string devicename, bool compressed)
{
    std::string archivename = "";
    if(compressed)
    {
        archivename = filename;
        filename = filename.substr(0, filename.rfind('.')) + ".xml";
    }

    //create a new XML Document:
    tinyxml2::XMLDocument doc;

    //include the XML declaration:
    tinyxml2::XMLDeclaration* dec = doc.NewDeclaration("xml version=\"1.0\"");
    doc.LinkEndChild(dec);

    tinyxml2::XMLElement* node;

    node = SaveToXMLElement(doc, devicename);

    if(node != nullptr)
        doc.LinkEndChild(node);

    tinyxml2::XMLError error = doc.SaveFile(filename.c_str());

    if(compressed)
    {
        zip_file archive;
        archive.write(filename, filename);

        archive.save(archivename);

        if(remove(filename.c_str()) != 0)
            std::cout << "error deleting compression helper file \"" << filename << "\""
                      << std::endl;
    }

    return error;
}

bool SPINexys::WriteNoNexysCheck(NexysIO *nexys, int setting, bool flush)
{
    bool notdone = true;
    bool success = true;
    int counter = 1000;
    while(notdone && --counter > 0)
    {
        if(buffercontent + wordsize <= buffersize)
        {
            success &= WriteNoCheck(nexys, setting, flush);
            notdone = false;
        }
        else
        {
            int result = nexys->Read(configurationaddress, 1).c_str()[0];
            if((result & fm_config.GetFirstBitPosition("WrFIFOEmpty")) == 0)
                buffercontent = 0;
            else if((result & fm_config.GetFirstBitPosition("WrFIFOFull")) == 0)
                buffercontent = buffersize - wordsize;

            if(buffercontent + wordsize <= buffersize)
            {
                success &= WriteNoCheck(nexys, setting);
                notdone = false;
            }
        }
    }

    return success && !notdone;
}

bool SPINexys::WriteNoCheck(NexysIO *nexys, int setting, bool flush)
{
    std::vector<byte> config;
    config.push_back(static_cast<byte>(setting >> 24));
    config.push_back(static_cast<byte>(setting >> 16));
    config.push_back(static_cast<byte>(setting >>  8));
    config.push_back(static_cast<byte>(setting      ));

    buffercontent += wordsize;

    return nexys->Write(writeaddress, config, flush);
}

bool SPINexys::WriteNoCheck(NexysIO *nexys, std::vector<int> data)
{
    std::vector<byte> config;
    bool result = true;

    for(const auto& it : data)
    {
        config.push_back(static_cast<byte>(it >> 24));
        config.push_back(static_cast<byte>(it >> 16));
        config.push_back(static_cast<byte>(it >>  8));
        config.push_back(static_cast<byte>(it      ));

        buffercontent += wordsize;

        if(config.size() >= 65530)
        {
            result &= nexys->Write(writeaddress, config, true);
            config.clear();
        }
    }

    if(config.size() > 0)
        result &= nexys->Write(writeaddress, config, true);

    return result;
}
