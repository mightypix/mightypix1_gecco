/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CMDCONFIG_H
#define CMDCONFIG_H

#include <vector>
#include "flagmanager.h"
#include "nexysio.h"

typedef unsigned int  uint;
typedef unsigned char byte;

class CMDConfig
{
public:
    CMDConfig();

    static const int syncword = 0b1000000101111110;
    static const int rwreg    = 0b0110011001100110;
    static const int setbit   = 0b0110010101100101;

    enum loads {
        lddac    =  16,
        ldconfig =  32,
        ldvdac   =  64,
        ldtdac   = 128,
        ldrow    = 256,
        ldcolumn = 512
    };

    ///-- Configuration of the FPGA Module --

    int  GetModuleConfigFPGAAddress() const;
    void SetModuleConfigFPGAAddress(int address);

    bool GetModuleEnable() const;
    void SetModuleEnable(bool enable, NexysIO* nexys = nullptr);

    bool GetModuleReset() const;
    void SetModuleReset(bool reset, NexysIO* nexys = nullptr);

    bool GetFIFOReset() const;
    void SetFIFOReset(bool reset, NexysIO* nexys = nullptr);

    bool QueryFIFOEmpty(NexysIO* nexys);
    bool QueryFIFOFull(NexysIO* nexys);

    ///-- End Configuration of the FPGA Module --

    bool GetConfigBit(std::string name);
    bool SetConfigBit(std::string name, bool value);

    int GetTriggerCode(bool trigger_slot1, bool trigger_slot2, bool trigger_slot3, bool trigger_slot4);
    int GetTriggerCode(int triggerpattern);

    std::vector<byte> GenerateSetBitByteCode(int chipid, int configreg);
    std::vector<byte> GenerateTriggerCode(int triggerpattern, int triggertag);
    std::vector<byte> GenerateRegWriteByteCode(int chipid, int bits);
    std::vector<byte> GenerateRegWriteByteCode(int chipid, std::vector<bool> bitstream,
                                               int loadconfig, int syncspacing = 50);
    std::vector<byte> GenerateManualRegWriteByteCode(int chipid, std::vector<bool> bitstream,
                                                     int loadconfig, const int syncspacing = 4);

private:
    std::vector<int> encoder;
    FlagManager config0;    //configuration inside the chip, register 0
    FlagManager config1;    //configuration inside the chip, register 1

    FlagManager moduleconfig; //configuration of the FPGA module, not the chip

    void AddSyncWord(std::vector<byte> *vec, int numwords);
    void AddSetBitCMD(std::vector<byte> *vec, int chipid, int pattern, int syncspacing);
    void AddRegWriteWord(std::vector<byte> *vec, int chipid, int pattern, int syncspacing);
};

#endif // CMDCONFIG_H
